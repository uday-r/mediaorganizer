package uday.controller.copyInitiator;

import java.io.File;
import java.io.IOException;



import org.apache.commons.io.FilenameUtils;

import uday.Utils.fileOperations.MediaCopy;
import uday.exceptions.MediaCopyException;
import uday.exceptions.MediaFileException;
import uday.model.domainModel.MediaSourceDetails;
import uday.model.globalVariables.GlobalVariables;
import uday.service.resourceService.ResourceBundleService;

public class MediaFileCopyInitiator {

	private MediaSourceDetails mediaSourceDetails;
	
	public MediaFileCopyInitiator(MediaSourceDetails mediaSourceDetails) throws InterruptedException {
		this.mediaSourceDetails = mediaSourceDetails;
	}


	public void prepareForCopy(String sourceFilePath,
			String destinationFilePath) throws  MediaCopyException,  IOException, MediaFileException{
		File sourceFile = null;
		sourceFile = new File(sourceFilePath);
		if(!sourceFile.exists()){
			throw new MediaFileException(ResourceBundleService.getLabel("sourceMediaNotFound"));
		}

		if(!new File(GlobalVariables.getMediaOrganizerPath()+GlobalVariables.getMovieFolder()).exists()){
			new File(GlobalVariables.getMediaOrganizerPath()+GlobalVariables.getMovieFolder()).mkdirs();
		}
		if (!sourceFile.isDirectory()) {
			destinationFilePath = destinationFilePath + FilenameUtils.removeExtension(sourceFile.getName());
			File destinationFile = new File(destinationFilePath);
			destinationFile.mkdir();
		}

		MediaCopy.setFilesCopied(0);
		MediaCopy.startCopy(sourceFilePath, destinationFilePath, mediaSourceDetails);
			
	
		int sourceFileSelectedToCopy = 0;
		if(mediaSourceDetails.getAudioFile()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.getVideoFile()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.getPoster()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.getSubtitle()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.geteBookFile()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}

		if(sourceFileSelectedToCopy != MediaCopy.getFilesCopied()){
			throw new MediaCopyException(ResourceBundleService.getLabel("internalProbelmFileNotCopied"));
		}
	}

	

    
	public MediaSourceDetails getMediaSourceDetails() {
		return mediaSourceDetails;
	}


	public void setMediaSourceDetails(MediaSourceDetails mediaSourceDetails) {
		this.mediaSourceDetails = mediaSourceDetails;
	}
	
}
