package uday.controller.editMedia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import uday.view.editMediaDetails.MediaFileMissing;
import uday.view.mediaOrganizer.Button;


public class MediaMissingAction implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent event) {
		Button button = (Button)event.getSource();
		MediaFileMissing mediaFileMissing = (MediaFileMissing) button.getTopLevelAncestor();
		mediaFileMissing.dispose();
	}

}
