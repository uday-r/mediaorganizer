package uday.controller.editMedia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import uday.view.editMediaDetails.EditMovieDetailsView;


public class EditMovieDetailsCancelButton implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		EditMovieDetailsView editMovieDetailsView = (EditMovieDetailsView) ((JButton) event.getSource()).getTopLevelAncestor();
		editMovieDetailsView.dispose();
	}
}
