package uday.controller.mediaOrganizer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;

import uday.Utils.ErrorMessage;
import uday.exceptions.ConnectionNotAvaliableException;
import uday.model.domainModel.MediaFilter;
import uday.model.domainModel.MediaTypeDetails;
import uday.service.databaseService.MediaOrganizerDatabaseService;
import uday.view.mediaOrganizer.MediaOrganizerView;
import uday.view.mediaOrganizer.SearchTextField;



public class SearchByTextBoxKeyListener extends Observable implements KeyListener {

	private MediaOrganizerDatabaseService getDatabaseInstance()
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, SQLException {
		MediaOrganizerDatabaseService mediaOrganizerDatabaseService = MediaOrganizerDatabaseService
				.getMediaOrganizerInstance();
		return mediaOrganizerDatabaseService;
	}

	@Override
	public void keyPressed(KeyEvent event) {
		

	}
	@Override
	public void keyTyped(KeyEvent event) {

	}
	
	
	@Override
	public void keyReleased(KeyEvent event) {
		try {
			MediaOrganizerDatabaseService mediaOrganizerDatabaseService = getDatabaseInstance();
			
			SearchTextField searchByTextField = (SearchTextField) event.getSource();
			
			
			MediaOrganizerView mediaOrganizerView = (MediaOrganizerView)searchByTextField.getTopLevelAncestor();
			String media = searchByTextField.getText();
			String mediaType = mediaOrganizerView.getMediaType();
			String mediaTypeProperty = mediaOrganizerView.getMediaTypeProperty();
			MediaTypeDetails mediaTypeDetails = new MediaTypeDetails(mediaType, mediaTypeProperty, media);
			ArrayList<ArrayList<String>> searchResults;
			searchResults = mediaOrganizerDatabaseService.getSearchedMedia(mediaTypeDetails);
			MediaFilter mediaFilter = new MediaFilter(mediaTypeDetails, searchResults);
			setChanged();
			notifyObservers(mediaFilter);

		} catch (InstantiationException e){
			ErrorMessage.getErrorMessage();
		} catch(IllegalAccessException e){
			ErrorMessage.getErrorMessage();
		} catch(ClassNotFoundException e){
			ErrorMessage.getErrorMessage();
		} catch(SQLException e1) {
			ErrorMessage.getDataBaseErrorMessage("Something wrong with database, please try again later!");
		} catch (ConnectionNotAvaliableException e) {
			ErrorMessage.getDataBaseErrorMessage("Too many connection to database please try again later!");
		}
	}

	
}
