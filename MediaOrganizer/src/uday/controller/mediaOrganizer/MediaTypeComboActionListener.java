package uday.controller.mediaOrganizer;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;

import uday.Utils.ErrorMessage;
import uday.exceptions.ConnectionNotAvaliableException;
import uday.model.domainModel.MediaFilter;
import uday.model.domainModel.MediaTypeDetails;
import uday.service.databaseService.MediaOrganizerDatabaseService;
import uday.view.mediaOrganizer.MediaTypeComboBox;



public class MediaTypeComboActionListener extends Observable implements
		ActionListener {

	@Override
	public void actionPerformed(ActionEvent event) {

		MediaTypeComboBox mediaTypeComboBox = (MediaTypeComboBox) event
				.getSource();
		
		
		String media = null;
		String mediaType = (String) mediaTypeComboBox.getSelectedItem();
		String mediaTypeProperty = null;
		MediaTypeDetails mediaTypeDetails = new MediaTypeDetails(mediaType, mediaTypeProperty, media);
		ArrayList<ArrayList<String>> searchResults = null;
		MediaOrganizerDatabaseService mediaOrganizerDatabaseService;
		try {
			mediaOrganizerDatabaseService = MediaOrganizerDatabaseService.getMediaOrganizerInstance();
			searchResults = mediaOrganizerDatabaseService.getSearchedMedia(mediaTypeDetails);
		} catch (InstantiationException e){
			ErrorMessage.getErrorMessage();
		} catch (IllegalAccessException e){
			ErrorMessage.getErrorMessage();

		} catch( ClassNotFoundException e){
			ErrorMessage.getErrorMessage();
		} catch( SQLException e) {
			ErrorMessage.getDataBaseErrorMessage("Something wrong with database, please try again later!");
		} catch (ConnectionNotAvaliableException e) {
			ErrorMessage.getDataBaseErrorMessage("Too many connection to database please try again later!");
		}
		MediaFilter mediaFilter = new MediaFilter(mediaTypeDetails, searchResults);
		
		
		
		setChanged();
		notifyObservers(mediaFilter);
	}

}
