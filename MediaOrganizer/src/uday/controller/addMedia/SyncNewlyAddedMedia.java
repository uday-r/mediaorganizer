package uday.controller.addMedia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import uday.Utils.ErrorMessage;
import uday.Utils.inputValidation.MediaInputValidate;
import uday.exceptions.ConnectionNotAvaliableException;
import uday.exceptions.MediaCopyException;
import uday.exceptions.MediaFileDuplicationException;
import uday.exceptions.MediaFileException;
import uday.exceptions.ValidationException;
import uday.model.domainModel.FileCopyStatus;
import uday.model.domainModel.MediaSourceDetails;
import uday.model.viewModel.AddMediaFilesTableModel;
import uday.service.databaseService.MediaOrganizerDatabaseService;
import uday.service.resourceService.ResourceBundleService;
import uday.view.addNewMedia.AddMediaFilesTable;
import uday.view.addNewMedia.AddNewMediaView;
import uday.view.operationProgress.OperationProgress;

public class SyncNewlyAddedMedia implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent event) {
		Boolean dispose = false;
		AddMediaFilesTableModel addedMediaFiles = getAddMediaFilesTableModel(event);
		AddMediaFilesTable addMediaFilesTable = getAddMediaFilesTable(event);

		ArrayList<MediaSourceDetails> mediaSourceDetailsList = new ArrayList<MediaSourceDetails>();

		for (int i = 0; i < addedMediaFiles.getRowCount(); i++) {

			ArrayList<String> row = new ArrayList<String>();

			row.add((String) addedMediaFiles.getValueAt(i, 0));
			row.add((String) addedMediaFiles.getValueAt(i, 1));
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 2)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 3)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 4)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 5)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 6)).toString());
			row.add((String) addedMediaFiles.getValueAt(i, 7));
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 9)).toString());
			mediaSourceDetailsList.add(new MediaSourceDetails(row));
		}
		try {

			MediaOrganizerDatabaseService databaseServiceInstance = MediaOrganizerDatabaseService
					.getMediaOrganizerInstance();

			OperationProgress operationProgress = null;
			JProgressBar progressBar = null;
			Boolean initiateCopy = true;
			if (!mediaSourceDetailsList
					.get(0)
					.getCopyStatus()
					.equals(ResourceBundleService
							.getLabel("failureFileAlreadyFound"))) {
				operationProgress = new OperationProgress();
				progressBar = operationProgress.getProgressBar();
				progressBar.setValue(0);
				initiateCopy = true;
			}

			int fileCount = 0;

			ArrayList<Integer> removeMediaSourceDetailsIndex = new ArrayList<Integer>();

			for (MediaSourceDetails mediaSourceDetails : mediaSourceDetailsList) {
				try {
					MediaInputValidate.getMediaType(mediaSourceDetails);

					if (mediaSourceDetails.getMediaType().equals(
							ResourceBundleService.getLabel("movieComboBox"))) {
						try {
							databaseServiceInstance
									.insertNewMovie(mediaSourceDetails);
							removeMediaSourceDetailsIndex
									.add(mediaSourceDetailsList
											.indexOf(mediaSourceDetails));
							dispose = true;
						} catch (SQLException e) {
							ErrorMessage
									.getDataBaseErrorMessage(e.getMessage());
							mediaSourceDetails = setErrorMessage(
									event,
									mediaSourceDetailsList,
									mediaSourceDetails,
									new FileCopyStatus(
											ResourceBundleService
													.getLabel("error"),
											ResourceBundleService
													.getLabel("errorInDatabase")),
									e);

						} catch (MediaCopyException e) {
							ErrorMessage.getFileNotCopied(e.getMessage());
							mediaSourceDetails = setErrorMessage(
									event,
									mediaSourceDetailsList,
									mediaSourceDetails,
									new FileCopyStatus(ResourceBundleService
											.getLabel("error"),
											ResourceBundleService
													.getLabel("fileNotCopied")),
									e);
						} catch (MediaFileDuplicationException e) {
							addMediaFilesTable.hideFolderDetailColumns();
							mediaSourceDetails = setErrorMessage(
									event,
									mediaSourceDetailsList,
									mediaSourceDetails,
									new FileCopyStatus(
											ResourceBundleService
													.getLabel("error"),
											ResourceBundleService
													.getLabel("failureFileAlreadyFound")),
									e);

							getAddMediaFilesTableModel(event).setValueAt(
									new Boolean(true),
									mediaSourceDetailsList
											.indexOf(mediaSourceDetails), 9);
							mediaSourceDetails.setOverwriteMedia(new Boolean(
									true));
							mediaSourceDetailsList.set(mediaSourceDetailsList
									.indexOf(mediaSourceDetails),
									mediaSourceDetails);
							ErrorMessage.getMediaFileDuplicationErrorMessage(e
									.getMessage());
						} catch (ConnectionNotAvaliableException e) {
							ErrorMessage
									.getDataBaseErrorMessage(e.getMessage());

						} catch (IOException e) {
							ResourceBundleService
									.getLabel("filePermossionError");
						} catch (MediaFileException e) {
							ErrorMessage.getMediaFileNotFoundErrorMessage(e
									.getMessage());
						}

					}
					fileCount = fileCount + 1;
					if (initiateCopy) {
						int allFiles = fileCount;

						progressBar.setValue(fileCount * 100 / allFiles);
						if (progressBar.getValue() == 100) {
							operationProgress.dispose();
						}
					}
				} catch (ValidationException e1) {
					ErrorMessage.getIncorrectInput();
					dispose = false;
				}
			}

			deleteSuccessRows(removeMediaSourceDetailsIndex, event);

		} catch (InstantiationException e) {
			ErrorMessage.getErrorMessage();
		} catch (IllegalAccessException e) {
			ErrorMessage.getErrorMessage();
		} catch (ClassNotFoundException e) {
			ErrorMessage.getErrorMessage();
		} catch (InterruptedException e) {
			ErrorMessage.getErrorMessage();
		} catch (SQLException e) {
			ResourceBundleService.getLabel("databaseConnectionError");
		}
		if (dispose) {
			closeAddMediaBox(event);
		}
	}

	private MediaSourceDetails setErrorMessage(ActionEvent event,
			ArrayList<MediaSourceDetails> mediaSourceDetailsList,
			MediaSourceDetails mediaSourceDetails,
			FileCopyStatus fileCopyStatus, Exception e) {

		mediaSourceDetails.setCopyStatus(fileCopyStatus);
		getAddMediaFilesTableModel(event).setValueAt(
				(Object) mediaSourceDetails.getCopyStatus().getStatusMessage(),
				mediaSourceDetailsList.indexOf(mediaSourceDetails), 8);

		return mediaSourceDetails;
	}

	private void deleteSuccessRows(
			ArrayList<Integer> removeMediaSourceDetailsIndex, ActionEvent event) {
		ListIterator<Integer> listIterator = removeMediaSourceDetailsIndex
				.listIterator(removeMediaSourceDetailsIndex.size());
		while (listIterator.hasPrevious()) {
			getAddMediaFilesTableModel(event).removeRow(
					(Integer) listIterator.previous());

		}
	}

	private void closeAddMediaBox(ActionEvent event) {
		AddNewMediaView addNewMediaView = (AddNewMediaView) ((JButton) event
				.getSource()).getTopLevelAncestor();
		addNewMediaView.dispose();
	}

	public AddMediaFilesTableModel getAddMediaFilesTableModel(ActionEvent event) {
		return (AddMediaFilesTableModel) ((AddMediaFilesTable) ((JScrollPane) ((JButton) event
				.getSource()).getParent().getParent().getComponents()[1])
				.getViewport().getComponents()[0]).getModel();
	}

	public AddMediaFilesTable getAddMediaFilesTable(ActionEvent event) {
		return ((AddMediaFilesTable) ((JScrollPane) ((JButton) event
				.getSource()).getParent().getParent().getComponents()[1])
				.getViewport().getComponents()[0]);
	}
}
