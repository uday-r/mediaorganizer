package uday.controller.addMedia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import uday.view.addNewMedia.AddNewMediaView;


public class AddNewCancelButton implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		AddNewMediaView addNewMediaView = (AddNewMediaView) ((JButton) event.getSource()).getTopLevelAncestor();
		addNewMediaView.dispose();
	}

}
