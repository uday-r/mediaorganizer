package uday.service.resourceService;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleService {
	private static String language;

	private static String country;

	private static Locale currentLocale;
	static ResourceBundle labels;
	static {
		language = new String("en");
		country = new String("US");
		currentLocale = new Locale(language, country);
		labels = ResourceBundle.getBundle("uday.properties.MessageBundle", currentLocale);
	}
	
	public static String getLabel(String resourceIndex){
		return labels.getString(resourceIndex);
	}
}
