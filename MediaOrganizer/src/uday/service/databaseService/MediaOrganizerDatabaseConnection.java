package uday.service.databaseService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import uday.model.globalVariables.GlobalVariables;


public class MediaOrganizerDatabaseConnection {
	private Connection mediaOrganizerDatabaseConnection = null;
	
	public MediaOrganizerDatabaseConnection() throws SQLException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		String connectionUrl = "jdbc:mysql://localhost:3306/"+GlobalVariables.getDatabasename();
		String connectionUser = "root";
		String connectionPassword = "8147312585";
		mediaOrganizerDatabaseConnection = DriverManager.getConnection(
				connectionUrl, connectionUser, connectionPassword);
	}
	
	public MediaOrganizerDatabaseConnection(Connection databaseConnection){
		this.mediaOrganizerDatabaseConnection = databaseConnection;
	}
	
	public Connection getMediaOrganizerDatabaseConnection(){
		return this.mediaOrganizerDatabaseConnection;
	}

	
}
