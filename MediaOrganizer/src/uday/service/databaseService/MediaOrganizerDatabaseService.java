package uday.service.databaseService;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import uday.controller.copyInitiator.MediaFileCopyInitiator;
import uday.exceptions.ConnectionNotAvaliableException;
import uday.exceptions.MediaCopyException;
import uday.exceptions.MediaFileDuplicationException;
import uday.exceptions.MediaFileException;
import uday.model.domainModel.MediaSourceDetails;
import uday.model.domainModel.MediaTypeDetails;
import uday.model.globalVariables.GlobalVariables;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;


public class MediaOrganizerDatabaseService {

	int temp = 0;
	private static MediaOrganizerDatabaseService mediaOrganizerDatabaseService = null;
	MediaOrganizerDatabaseConnectionPool mediaOrganizerDatabaseConnectionPool = null;

	private MediaOrganizerDatabaseService() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		mediaOrganizerDatabaseConnectionPool = new MediaOrganizerDatabaseConnectionPool();
	}

	public static MediaOrganizerDatabaseService getMediaOrganizerInstance()
			throws IllegalAccessException, ClassNotFoundException,
			SQLException, InstantiationException {
		if (mediaOrganizerDatabaseService == null) {
			synchronized (MediaOrganizerDatabaseConnection.class) {
				if (mediaOrganizerDatabaseService == null) {
					mediaOrganizerDatabaseService = new MediaOrganizerDatabaseService();
				}
			}
		}
		return mediaOrganizerDatabaseService;
	}

	private Connection getAvaliableConnection()
			throws ConnectionNotAvaliableException {
		
		MediaOrganizerDatabaseConnection mediaOrganizerDatabaseConnection = mediaOrganizerDatabaseConnectionPool
				.getAvaliableConnection();
		if (mediaOrganizerDatabaseConnection == null) {

			throw new ConnectionNotAvaliableException(
					"Database Connection not avaliable");
		}

		return (Connection) mediaOrganizerDatabaseConnection
				.getMediaOrganizerDatabaseConnection();
	}

	private void returnConnection(Connection databaseConnection) throws SQLException {
		mediaOrganizerDatabaseConnectionPool
				.returnConnection(databaseConnection);
	}

	private String replaceSpaceWithUnderscore(Object name) {
		return ((String) name).replaceAll(" ", "_");
	}

	private String mediaSearchQueryFactory(MediaTypeDetails mediaDetails) {
		Object mediaType = mediaDetails.getMediaType();
		String mediaTypeWithUnderScore = replaceSpaceWithUnderscore(mediaType);
		Object columnName = mediaDetails.getMediaTypeProperty();
		String columnTypename = null;
		if (columnName != null)
			columnTypename = replaceSpaceWithUnderscore(columnName);
		String listMoviesSearchedQuery = null;
		String media = mediaDetails.getMedia();
		if (media == null) {
			listMoviesSearchedQuery = "select Movie_Name ,Year ,Movie_Rating ,ImdbVotes ,Released ,Runtime ,Genres ,Directors ,Writer ,Actors ,Plot ,Poster ,ImdbID ,Type ,MediaOrganizerFilePath  from "
					+ mediaTypeWithUnderScore;
		} else {

			if (columnName == "Movie Rating") {
				if (mediaTypeWithUnderScore.isEmpty()) {
					listMoviesSearchedQuery = "select * from "
							+ mediaTypeWithUnderScore + " where "
							+ columnTypename + " <= 0;";
				} else {
					listMoviesSearchedQuery = "select * from "
							+ mediaTypeWithUnderScore + " where "
							+ columnTypename + " <= " + media + ";";
				}

			} else {
				listMoviesSearchedQuery = "select Movie_Name ,Year ,Movie_Rating ,ImdbVotes ,Released ,Runtime ,Genres ,Directors ,Writer ,Actors ,Plot ,Poster ,ImdbID ,Type ,MediaOrganizerFilePath from "
						+ mediaTypeWithUnderScore
						+ " where "
						+ columnTypename
						+ " like '%" + media + "%';";
			}
		}
		return listMoviesSearchedQuery;
	}

	private ResultSet executeSelectQuery(Connection connection, String query)
			throws ConnectionNotAvaliableException, SQLException {

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		return resultSet;

	}

	public ArrayList<ArrayList<String>> getSearchedMedia(
			MediaTypeDetails mediaDetails) throws SQLException,
			ConnectionNotAvaliableException {

		String listMoviesSearchedQuery = mediaSearchQueryFactory(mediaDetails);
		ArrayList<ArrayList<String>> allMoviesDetailsList = new ArrayList<ArrayList<String>>();

		Connection connection = getAvaliableConnection();
		connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet listMoviesSearchedResultSet = executeSelectQuery(connection,
				listMoviesSearchedQuery);

		while (listMoviesSearchedResultSet.next()) {
			ArrayList<String> movieDetails = new ArrayList<String>();
			ResultSetMetaData moviesMetaData = (ResultSetMetaData) listMoviesSearchedResultSet
					.getMetaData();
			int moviesColumns = moviesMetaData.getColumnCount();
			String moviesDetailColumn;
			for (int i = 1; i <= moviesColumns; i++) {
				moviesDetailColumn = listMoviesSearchedResultSet.getString(i);
				movieDetails.add(moviesDetailColumn);
			}
			allMoviesDetailsList.add(movieDetails);
		}
		listMoviesSearchedResultSet.close();
		returnConnection(connection);
		return allMoviesDetailsList;
	}

	private String searchIfNoMediaAvaliableQueryFactory(String mediaName,
			String mediaType) {
		return "select " + mediaType + "_Name from " + mediaType + " where "
				+ mediaType + "_Name like '" + mediaName + "';";
	}

	public int getMediaCount(String query) throws SQLException,
			ConnectionNotAvaliableException {
		Connection connection = getAvaliableConnection();

		ResultSet searchIfMediaAvaliableResultSet = executeSelectQuery(
				connection, query);
		searchIfMediaAvaliableResultSet.last();
		returnConnection(connection);
		return searchIfMediaAvaliableResultSet.getRow();
	}

	private boolean searchIfNoMediaAvaliable(String mediaName, String mediaType)
			throws SQLException, ConnectionNotAvaliableException {
		String searchIfNoMediaAvaliableQuery = searchIfNoMediaAvaliableQueryFactory(
				mediaName, mediaType);
		System.out.println("query: " + searchIfNoMediaAvaliableQuery);
		int rowCount = 0;
		ResultSet searchIfNoMediaAvaliableResultSet;
		Connection connection = getAvaliableConnection();
		connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		searchIfNoMediaAvaliableResultSet = executeSelectQuery(connection,
				searchIfNoMediaAvaliableQuery);

		searchIfNoMediaAvaliableResultSet.last();

		rowCount = searchIfNoMediaAvaliableResultSet.getRow();
		searchIfNoMediaAvaliableResultSet.close();
		returnConnection(connection);

		if (rowCount > 0) {
			return false;
		} else {
			return true;
		}
	}

	private void copyFiles(MediaSourceDetails mediaSourceDetails)
			throws InterruptedException, 
			MediaCopyException, IOException, MediaFileException {
		MediaFileCopyInitiator mediaFileOperationInitiator = new MediaFileCopyInitiator(
				mediaSourceDetails);
		mediaFileOperationInitiator.prepareForCopy(
				mediaSourceDetails.getMediaSourcePath(),
				GlobalVariables.getMediaOrganizerPath()
						+ GlobalVariables.getMovieFolder());
	}

	public void insertNewMovie(MediaSourceDetails mediaSourceDetails)
			throws SQLException, IOException, InterruptedException,
			MediaCopyException, MediaFileException,
			ConnectionNotAvaliableException, MediaFileDuplicationException {
		Connection connection = getAvaliableConnection();
		connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		String movieName = mediaSourceDetails.getMediaName();

		if (searchIfNoMediaAvaliable(movieName, "Movie")) {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			String query = "INSERT INTO  Movie (Movie_Name, MediaOrganizerFilePath) VALUES (?, ?)";
			PreparedStatement insertMovie = null;
			try {
				String movieDestinationPath = null;
					insertMovie = (PreparedStatement) connection
							.prepareStatement(query);
					movieDestinationPath = GlobalVariables
							.getMediaOrganizerPath()
							+ GlobalVariables.getMovieFolder() + movieName;

					insertMovie.setString(1, movieName);
					insertMovie.setString(2, movieDestinationPath);
					insertMovie.executeUpdate();

					copyFiles(mediaSourceDetails);

				connection.commit();
			} catch (SQLException e) {
				connection.rollback();
				throw new SQLException("Sql exception", e);
			} catch (MediaFileException e) {
				connection.rollback();
				throw new MediaFileException(e.getMessage(), e);
			} catch (MediaCopyException e) {
				connection.rollback();
				throw new MediaCopyException(e.getMessage(), e);
			} finally {
				connection.setAutoCommit(true);
				returnConnection(connection);
				if (insertMovie != null) {
					insertMovie.close();
				}

			}
		} else {
			if (mediaSourceDetails.getOverwriteMedia()) {
				copyFiles(mediaSourceDetails);
			} else {
				throw new MediaFileDuplicationException("Media Already Found in Media Organizer");
			}
		}
	}

	// public void deleteMedia(ArrayList<MediaSourceDetails>
	// mediaSourceDetailsList)
	// throws SQLException {
	// for (MediaSourceDetails mediaSourceDetails : mediaSourceDetailsList) {
	// if (mediaSourceDetails.getMediaType().equals("Movie")) {
	// deleteMovie(mediaSourceDetails);
	// } else if (mediaSourceDetails.getMediaType().equals("none")) {
	// mediaSourceDetails.setCopyStatus(new FileCopyStatus("Failure",
	// "Please add relevant Media Files"));
	// }
	//
	// }
	// }

	// @SuppressWarnings("unused")
	// private void deleteMovie(MediaSourceDetails mediaSourceDetails)
	// throws SQLException {
	//
	// String movieName = mediaSourceDetails.getMediaName();
	// FileCopyStatus fileCopyStatus = null;
	// Connection connection =
	// MediaOrganizerDatabaseService.mediaOrganizerDatabaseConnection;
	//
	// if (!searchIfNoMediaAvaliable(movieName, "Movie")) {
	//
	// String movieSourcePath = mediaSourceDetails.getMediaSourcePath();
	//
	// String query = "delete from Movie where Movie_Name = ?";
	//
	// PreparedStatement deleteMovie = null;
	//
	// deleteMovie = (PreparedStatement) connection
	// .prepareStatement(query);
	//
	// deleteMovie.setString(1, movieName);
	// deleteMovie.executeUpdate();
	//
	// MediaDelete.deleteFile(movieSourcePath);
	//
	// if (MediaDelete.filePresent(movieSourcePath)) {
	// connection.rollback();
	// } else {
	// connection.commit();
	// }
	// }
	//
	// }
}
