package uday.model.domainModel;

public class FileCopyStatus {

	private String status;
	private String message;

	public FileCopyStatus(String status, String message) {
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public String getStatusMessage() {
		return status + ":" + message;
	}
}
