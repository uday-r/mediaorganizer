package uday.model.domainModel;

import org.apache.log4j.Logger;

import uday.view.mediaOrganizer.Button;


public class TableRowSelectionResponse {

	static Logger log = Logger.getLogger(TableRowSelectionResponse.class.getName());
	private Boolean buttonEnabled;
	public TableRowSelectionResponse(TableRowData tableData, Boolean buttonEnabled){
		log.info(TableRowSelectionResponse.class.getName()+" constructor invoked.");
		this.buttonEnabled = buttonEnabled;
		log.debug("value of buttonEnabled is "+buttonEnabled);
	}
	
	public void setButtonEnabled(Boolean buttonEnabled){
		this.buttonEnabled = buttonEnabled;
		log.debug("value of buttonEnabled is "+buttonEnabled);
	}
	
	public Boolean getButtonEnabled(){
		return this.buttonEnabled;
	}
	
}
