package uday.model.domainModel;

public class MediaTypeDetails {

	private String mediaType;
	private String mediaTypeProperty;
	private String media;
	@Override
	public String toString(){
		return "Media Type: "+mediaType+"\t Media type property"+mediaTypeProperty+"\t Media: "+media;
	}
	public MediaTypeDetails(String mediaType, String mediaTypeProperty, String media){
		this.setMediaType(mediaType);
		this.setMediaTypeProperty(mediaTypeProperty);
		this.setMedia(media);
	}
	public String getMediaType() {
		return mediaType;
	}
	private void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	public String getMediaTypeProperty() {
		return mediaTypeProperty;
	}
	private void setMediaTypeProperty(String mediaTypeProperty) {
		this.mediaTypeProperty = mediaTypeProperty;
	}
	public String getMedia() {
		return media;
	}
	private void setMedia(String media) {
		this.media = media;
	}
	
	
}
