package uday.model.viewModel;

import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import uday.view.mediaOrganizer.RowSelectionButton;


public class ResultSetTableModel extends DefaultTableModel {

	/**
	 * 
	 */
	static Logger log = Logger.getLogger(ResultSetTableModel.class.getName());
	private static final long serialVersionUID = 1L;
	public ResultSetTableModel() {
		super();
		log.info(ResultSetTableModel.class.getName()+" constructor initiated");
		addAllMediaDetailsToColumn();
	}

	
	public void addAllMediaDetailsToColumn() {
		log.info("addAllMediaDetailsToColumn function encountered");
		removeAllColumns();
		addColumn("Media Name");
		log.debug("Add column Media Name");
		addColumn("Media Rating");
		log.debug("Add column Media Rating");
		addColumn("Release Data");
		log.debug("Add column Release Data");
		addColumn("Category");
		log.debug("Add column Category");
	}

	private void removeAllColumns() {
		
		setColumnCount(0);
		log.info("setColoumn count to 0");
	}

	public void addMovieDetailsToColumn() {
		
		log.info("addMovieDetailsToColumn function encountered");
		removeAllColumns();
		addColumn("Movie Name");
		log.debug("Add column "+"Movie Name");
		addColumn("Year");
		log.debug("Add column "+"Year");
		addColumn("Movie Rating");
		log.debug("Add column "+"Movie Rating");
		addColumn("Rating Count");
		log.debug("Add column "+"Rating Count");
		addColumn("Release Date");
		log.debug("Add column "+"Release Date");
		addColumn("Movie Length");
		log.debug("Add column "+"Movie Length");
		addColumn("Genre");
		log.debug("Add column "+"Genre");
		addColumn("Directors");
		log.debug("Add column "+"Directors");
		addColumn("Writers");
		log.debug("Add column "+"Writers");
		addColumn("Actors");
		log.debug("Add column "+"Actors");
		addColumn("Plot");
		log.debug("Add column "+"Plot");
		addColumn("Poster");
		log.debug("Add column "+"Poster");
		addColumn("IMDB ID");
		log.debug("Add column "+"IMDB ID");
		addColumn("Type");
		log.debug("Add column "+"Type");
		addColumn("Media Organizer File Path");
		log.debug("Add column "+"Media Organizer File Path");
	}
	

	public void addBookDetailsToColumn() {
		log.info("addMovieDetailsToColumn function encountered");
		removeAllColumns();
		addColumn("Book Name");
		log.debug("Add column "+"Book Name");
		addColumn("Rating");
		log.debug("Add column "+"Rating");
		addColumn("Category");
		log.debug("Add column "+"Category");
		addColumn("Authors");
		log.debug("Add column "+"Authors");
		addColumn("Rating Count");
		log.debug("Add column "+"Rating Count");
		addColumn("Publish Date");
		log.debug("Add column "+"Publish Date");
		addColumn("Image");
		log.debug("Add column "+"Image");
	}
	public void clearModel(){
		setRowCount(0);
		log.debug("set row count to zero");
	}
	
}
