package uday.Utils;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ErrorMessage {
	static JFrame frame = new JFrame();

	private static void messageBox(String message, String error) {
		JOptionPane.showMessageDialog(frame, message, error,
				JOptionPane.ERROR_MESSAGE);
	}

	public static void getDataBaseErrorMessage(String message) {
		messageBox(message,
				"Database error");
	}

	public static void getErrorMessage() {
		messageBox(
				"Sorry, there is some internal problem please try again later",
				"File System error");
	}

	public static void getIOErrorMessage(String errorMessage) {
		messageBox(errorMessage, "Error encountered");
	}


	public static void getFileNotCopied(String message) {
		messageBox(message,
				"File Not Copied");
	}

	public static void getMediaFileDuplicationErrorMessage(String message) {
		messageBox(
				message,
				"File Not Copied");
	}
	public static void getMediaFileNotFoundErrorMessage(String message) {
		messageBox(
				message,
				"File Not Copied");
	}
	public static void getIncorrectInput() {
		messageBox(
				"Please do not give incorrect input.",
				"Operation stopped");
	}
	public static void getNoSuchMethodException(){
		messageBox(
				"Some error occured please try again later",
				"Operation stopped");
	}
	
}
