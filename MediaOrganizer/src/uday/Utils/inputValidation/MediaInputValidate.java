package uday.Utils.inputValidation;


import uday.exceptions.ValidationException;
import uday.model.domainModel.MediaSourceDetails;
import uday.model.globalVariables.GlobalVariables;



public class MediaInputValidate {

	public static  void getMediaType(MediaSourceDetails mediaSourceDetails) throws ValidationException{
		
		String mediaType = mediaSourceDetails.getMediaType();

		Boolean audio = mediaSourceDetails.getAudioFile();
		Boolean video = mediaSourceDetails.getVideoFile();
		Boolean book = mediaSourceDetails.geteBookFile();
		
			if (mediaType.equals(GlobalVariables.getMovieAsMediaType())) {
				if(video != true){
					throw new ValidationException();
				}
			}
			
			if (mediaType.equals(GlobalVariables.getMusicAsMediaType())) {
				if(video == true || audio != true){
					throw new ValidationException();
				}
			}
			
			if (mediaType.equals(GlobalVariables.getBookAsMediaType())) {
				if(video == true || audio == true || book != true){
					throw new ValidationException();
				}
			}
		
	}
}
