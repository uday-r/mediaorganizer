package uday.exceptions;

public class MediaFileDuplicationException  extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	private Throwable e;
	
	public MediaFileDuplicationException(){
		super();
	}
	public MediaFileDuplicationException(String message){
		super(message);
		this.message = message;
	}
	
	public MediaFileDuplicationException(String message, Throwable e){
		super(message);
		this.message = message;
		this.setE(e);
	}

	@Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }
	public Throwable getE() {
		return e;
	}
	public void setE(Throwable e) {
		this.e = e;
	}


}
