package uday.exceptions;

public class MediaFileException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	private Throwable e;
	public MediaFileException(){
		super();
	}
	
	public MediaFileException(String message){
		super(message);
		this.message = message;
	}
	
	public MediaFileException(String message, Throwable e){
		super(message);
		this.message = message;
		this.e = e;
	}

	@Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }

}
