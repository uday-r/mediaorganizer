package uday.exceptions;

public class ValidationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public ValidationException(){
		super();
	}
	
	public ValidationException(String message){
		super(message);
		this.message = message;
	}

	@Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }

}
