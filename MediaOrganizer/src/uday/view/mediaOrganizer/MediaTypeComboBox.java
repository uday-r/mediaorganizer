package uday.view.mediaOrganizer;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import org.apache.log4j.Logger;

import uday.service.resourceService.ResourceBundleService;

public class MediaTypeComboBox extends JComboBox {
	/**
	 * 
	 */
	static Logger log = Logger.getLogger(RowSelectionButton.class.getName());
	private static final long serialVersionUID = 1L;
	private static DefaultComboBoxModel mediaTypeComboBoxModel = new DefaultComboBoxModel(
			new Object[] { ResourceBundleService.getLabel("allMediaComboBox"),
					"Movie",
					ResourceBundleService.getLabel("bookComboBox") });

	public MediaTypeComboBox() {
		super(mediaTypeComboBoxModel);
		log.info(RowSelectionButton.class.getName() + " constructor invoked");
		setSelectedItem("All_Media");
		log.debug("set Selected Item to 'All Media'");
	}

}
