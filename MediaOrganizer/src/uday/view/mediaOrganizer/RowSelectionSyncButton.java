package uday.view.mediaOrganizer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;

public class RowSelectionSyncButton extends RowSelectionButton implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Logger log = Logger.getLogger(RowSelectionSyncButton.class.getName());

	public RowSelectionSyncButton(String buttonName) {
		super(buttonName);
		log.info(RowSelectionSyncButton.class.getName()+" constructor invoked");
	}

	@Override
	public void update(Observable arg0, Object buttonEnableStatus) {
		log.info("Update block entered");
		@SuppressWarnings("unchecked")
		ArrayList<Boolean> buttonEnabledStatus = (ArrayList<Boolean>) buttonEnableStatus;
		setEnabled(buttonEnabledStatus.get(1));
		log.debug("change status to: "+buttonEnabledStatus.get(1));
		
	}

}
