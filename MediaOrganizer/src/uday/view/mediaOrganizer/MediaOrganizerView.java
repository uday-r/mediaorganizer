	package uday.view.mediaOrganizer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.apache.log4j.Logger;

import uday.controller.editMedia.EditMediaDetails;
import uday.controller.mediaOrganizer.AddMediaButtonActionListener;
import uday.controller.mediaOrganizer.MediaOrganizerController;
import uday.controller.mediaOrganizer.MediaTypeComboActionListener;
import uday.controller.mediaOrganizer.ResultSetListSelectionListener;
import uday.controller.mediaOrganizer.SearchByComboBoxActionListener;
import uday.controller.mediaOrganizer.SearchByTextBoxFocusListener;
import uday.controller.mediaOrganizer.SearchByTextBoxKeyListener;
import uday.service.resourceService.ResourceBundleService;

public class MediaOrganizerView extends JFrame {

	/**
	 * 
	 */
	static Logger log = Logger.getLogger(MediaOrganizerView.class.getName());

	private static final long serialVersionUID = 1L;
	JPanel headerPanel;
	JPanel bodyPanel;
	JPanel searchPanel;
	JPanel resultPanel;
	JPanel searchBoxPanel;
	JPanel mainPanelContainer;
	JPanel mainPanel;

	Button addMediaButton;
	RowSelectionEditButton editButton;
	JButton removeButton;
	RowSelectionSyncButton syncButton;
	MediaTypeComboBox mediaTypeComboBox;
	SearchTextField searchBox;
	SearchByComboBox searchByComboBox;
	ResultSetTable table;

	ResultSetListSelectionListener resultSetListSelectionListener;
	
	

	public MediaOrganizerView() {
		super(ResourceBundleService.getLabel("mediaOrganizerFrameName"));
		log.info(MediaOrganizerView.class.getName() + " constructor Initiated.");
		drawComponents();
		setVisible(true);
		setSize(700, 500);
	}

	private void drawComponents() {
		log.info("Start drawing components.");
		createPanels();
		addLayoutManagerToPanels();

		createComponentsInHeaderPanel();
		addComponentsToHeaderPanel();

		searchPanel.setBorder(BorderFactory.createEmptyBorder(40, 25, 0, 25));
		log.debug("Set border to search panel");

		createComponentsInSearchPanel();

		addComponentsToSearchBoxPanel();

		searchByComboBox.setMaximumSize(searchByComboBox.getPreferredSize());
		log.debug("Set maximum size of search panel to "
				+ searchByComboBox.getPreferredSize());
		addComponentsToSearchPanel();
		searchBoxPanel.setAlignmentY(0);
		searchByComboBox.setAlignmentY(0);
		log.debug("Set AlignmentY of search panel to 0");
		log.debug("Set AlignmentY of search By Combo Box to 0");
		createComponentsInResultPanel();

		addComponentsToResultsPanel();
		addComponentsToBodyPanel();
		addComponentsToMainPanelContainer();

		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		log.debug("add border to main panel");
		addComponentsToMainPanel();

		bindActionListeners();
		add(mainPanel);
	}

	public String getMediaType() {
		return (String) mediaTypeComboBox.getSelectedItem();
	}

	public ResultSetTable getresultSetTable() {
		return this.table;
	}

	public String getMediaTypeProperty() {
		return (String) searchByComboBox.getSelectedItem();
	}

	private void addComponentsToSearchBoxPanel() {
		searchBoxPanel.add(searchBox, BorderLayout.NORTH);

	}

	private void addComponentsToSearchPanel() {
		searchPanel.add(searchBoxPanel);
		searchPanel.add(Box.createRigidArea(new Dimension(50, 10)));
		searchPanel.add(searchByComboBox);

	}

	private void addComponentsToMainPanel() {
		mainPanel.add(mainPanelContainer);

	}

	private void addComponentsToMainPanelContainer() {
		mainPanelContainer.add(headerPanel, BorderLayout.NORTH);
		mainPanelContainer.add(bodyPanel, BorderLayout.CENTER);

	}

	private void createComponentsInHeaderPanel() {
		log.info("Create components in header panel");
		addMediaButton = new Button(ResourceBundleService.getLabel("addButton"));
		editButton = new RowSelectionEditButton(ResourceBundleService.getLabel("editButton"));
		removeButton = new JButton(ResourceBundleService.getLabel("deleteButton"));
		syncButton = new RowSelectionSyncButton(ResourceBundleService.getLabel("syncButton"));
		mediaTypeComboBox = new MediaTypeComboBox();
	}

	private void createComponentsInSearchPanel() {
		log.info("create components in search panel");
		searchBox = new SearchTextField(10);
		searchByComboBox = new SearchByComboBox();
	}

	private void bindActionListeners() {
		addActionListenerToAddMediaComboBox();
		addActionListenerToMediaTypeComboBox();
		addActionListenerToSearchByComboBox();
		addKeyListenerToSearchBox();
		addFocusListenerToSearchBox();
		resultSetListSelectionListener = new ResultSetListSelectionListener(
				table);
		activateEditButton();
		editMediaDetails();
	}

	private void editMediaDetails() {

		EditMediaDetails editMediaDetails = new EditMediaDetails();
		editButton.addActionListener(editMediaDetails);
	}

	private void activateEditButton() {

		resultSetListSelectionListener.addObserver(editButton);
		resultSetListSelectionListener.addObserver(syncButton);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel
				.addListSelectionListener(resultSetListSelectionListener);
	}

	private void addActionListenerToAddMediaComboBox() {
		log.info("Add Media Combo Box Listener");
		AddMediaButtonActionListener addMediaButtonActionListener = new AddMediaButtonActionListener();
		addMediaButton.addActionListener(addMediaButtonActionListener);
	}

	private void addFocusListenerToSearchBox() {
		SearchByTextBoxFocusListener searchByTextBoxFocusListener = new SearchByTextBoxFocusListener();
		searchBox.addFocusListener(searchByTextBoxFocusListener);
	}

	private void addKeyListenerToSearchBox() {
		SearchByTextBoxKeyListener searchByTextBoxKeyListener = new SearchByTextBoxKeyListener();
		searchByTextBoxKeyListener.addObserver(table);
		searchBox.addKeyListener(searchByTextBoxKeyListener);

	}

	private void addActionListenerToSearchByComboBox() {
		SearchByComboBoxActionListener searchByComboBoxActionListener = new SearchByComboBoxActionListener();
		searchByComboBoxActionListener.addObserver(searchBox);
		searchByComboBox.addActionListener(searchByComboBoxActionListener);
	}

	private void addActionListenerToMediaTypeComboBox() {

		MediaTypeComboActionListener mediaTypeComboActionListener = new MediaTypeComboActionListener();
		mediaTypeComboActionListener.addObserver(searchByComboBox);
		mediaTypeComboActionListener.addObserver(table);
		mediaTypeComboBox.addActionListener(mediaTypeComboActionListener);

	}

	private void createComponentsInResultPanel() {

		table = new ResultSetTable();
	}

	private void addComponentsToResultsPanel() {
		resultPanel.add(new JScrollPane(table));

	}

	private void addComponentsToHeaderPanel() {
		log.info("add components to header panel");
		;
		headerPanel.add(addMediaButton);
		headerPanel.add(editButton);
		headerPanel.add(removeButton);
		headerPanel.add(syncButton);
		headerPanel.add(mediaTypeComboBox);

	}

	private void addComponentsToBodyPanel() {
		bodyPanel.add(searchPanel);
		bodyPanel.add(resultPanel);

	}

	private void createPanels() {
		log.info("Create Panels");
		headerPanel = new JPanel();
		bodyPanel = new JPanel();
		searchPanel = new JPanel();
		resultPanel = new JPanel();
		searchBoxPanel = new JPanel();
		mainPanelContainer = new JPanel();
		mainPanel = new JPanel();

	}

	private void addLayoutManagerToPanels() {
		log.info("Add Layout to panels.");
		headerPanel.setLayout(new GridLayout(1, 5));
		bodyPanel.setLayout(new BoxLayout(bodyPanel, BoxLayout.Y_AXIS));
		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.X_AXIS));
		searchBoxPanel.setLayout(new BorderLayout(10, 10));
		resultPanel.setLayout(new BorderLayout());
		mainPanelContainer.setLayout(new BorderLayout());
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

	}
}
