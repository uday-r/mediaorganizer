package uday.view.editMediaDetails;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import uday.controller.editMedia.MediaMissingAction;

public class MediaFileMissing extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JLabel addMissingMediaLabel;
	private JButton cancelButton;
	private JPanel buttonPanel;
	private JPanel labelPanel;

	public MediaFileMissing(String mediaName){
		super("File Missing!!!");
		drawComponents(mediaName);
		setVisible(true);
		setSize(381, 100);
//		setResizable(false);
	}
	
	private void drawComponents(String mediaName){
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		addMissingMediaLabel = new JLabel("<html>"+mediaName + " is missing in MediaOrganizer please add it.</html>");
		labelPanel = new JPanel();
		labelPanel.add(addMissingMediaLabel);
		labelPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		mainPanel.add(labelPanel, BorderLayout.NORTH);
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(Box.createHorizontalGlue());
		cancelButton = new JButton("Ok");
		MediaMissingAction mediaFileMissing = new MediaMissingAction();
		cancelButton.addActionListener(mediaFileMissing);
		buttonPanel.add(cancelButton);
		buttonPanel.add(Box.createHorizontalGlue());
		
		mainPanel.add(buttonPanel, BorderLayout.CENTER);
		add(mainPanel, BorderLayout.CENTER);
	}
	
	public static void main(String[] args){
		new MediaFileMissing("Matrix");
	}
}
