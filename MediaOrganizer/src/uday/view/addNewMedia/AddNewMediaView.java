package uday.view.addNewMedia;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import uday.Utils.fileOperations.ImageOperation;
import uday.controller.addMedia.AddLabelMouseListener;
import uday.controller.addMedia.AddNewCancelButton;
import uday.controller.addMedia.DeleteLabelMouseListener;
import uday.controller.addMedia.SyncNewlyAddedMedia;
import uday.model.viewModel.AddMediaFilesTableModel;


public class AddNewMediaView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mediaType;
	private JPanel mainBorderPanel;
	private JPanel headerPanel;
	private JPanel bodyPanel;
	private JPanel footerPanel;

	private JLabel addImageLabel;
	private JLabel deleteImageLabel;

	private ImageIcon addImage;
	private ImageIcon deleteImage;
	private JCheckBox metaData;
	private JButton saveButton;
	private JButton cancelButton;
	private AddMediaFilesTable table;
	private JScrollPane scrollTable;

	public AddNewMediaView() {
		super("Add Media");

		setMediaType(mediaType);
		drawComponents();
		bindActionListeners();
		setVisible(true);
		setSize(600, 400);
		setLocation(300, 100);

	}

	private void bindActionListeners() {
		cancelButtonListener();
	}

	private void cancelButtonListener() {
		AddNewCancelButton addNewCancelButton = new AddNewCancelButton();
		cancelButton.addActionListener(addNewCancelButton);
	}

	private void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public String getMediaType() {
		return this.mediaType;
	}

	private void setFooterPanelBorder() {
		footerPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));

	}

	private void setHeaderPanelBorder() {
		headerPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
	}

	private void createComponents() {
		this.mainBorderPanel = new JPanel();
		this.headerPanel = new JPanel();
		addImage = ImageOperation.createImageIcon("Images/add.png");
		deleteImage = ImageOperation.createImageIcon("Images/delete.png");

		AddMediaFilesTableModel tableModel = new AddMediaFilesTableModel(
				getMediaType());
		table = new AddMediaFilesTable(tableModel);
		scrollTable = new JScrollPane(table);
		setBodyPanel(new JPanel());
		footerPanel = new JPanel();
		saveButton = new JButton("Save");
		cancelButton = new JButton("cancel");
		metaData = new JCheckBox("Delete Source File", true);
	}

	private void setLayoutTocreatedComponents() {
		getContentPane().setLayout(
				new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		mainBorderPanel.setLayout(new BorderLayout());
		headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.LINE_AXIS));
		footerPanel.setLayout(new BoxLayout(footerPanel, BoxLayout.X_AXIS));

	}

	private void addComponentsToHeaderPanel() {
		String headerLabelName = getMediaType();
		headerPanel.add(new JLabel("Add " + headerLabelName));
		Box.createRigidArea(new Dimension(10, 0));
		headerPanel.add(Box.createHorizontalGlue());
		addImageLabel = new JLabel(addImage);
		headerPanel.add(addImageLabel);
		deleteImageLabel = new JLabel(deleteImage);
		headerPanel.add(deleteImageLabel);

	}

	private void addComponentsToFooterPanel() {
		footerPanel.add(metaData);
		footerPanel.add(Box.createHorizontalGlue());
		footerPanel.add(saveButton);
		footerPanel.add(cancelButton);

	}

	private void addComponentsToMainBorderPanel() {
		mainBorderPanel.add(headerPanel, BorderLayout.NORTH);
		mainBorderPanel.add(scrollTable, BorderLayout.CENTER);
		mainBorderPanel.add(footerPanel, BorderLayout.SOUTH);

	}

	private void drawComponents() {
		createComponents();
		setLayoutTocreatedComponents();
		setMainPanelBorder();
		setHeaderPanelBorder();
		setFooterPanelBorder();

		addComponentsToHeaderPanel();

		addComponentsToMainBorderPanel();

		addComponentsToFooterPanel();

		add(mainBorderPanel);
		addActionListenersToComponents();

	}

	private void addLabelMouseListener() {
		AddLabelMouseListener addLabelMouseListener = new AddLabelMouseListener();
		addImageLabel.addMouseListener(addLabelMouseListener);
	}

	private void deleteLabelMouseListener() {
		DeleteLabelMouseListener deleteLabelMouseListener = new DeleteLabelMouseListener();
		deleteImageLabel.addMouseListener(deleteLabelMouseListener);
	}

	private void addActionListenersToComponents() {
		addLabelMouseListener();
		deleteLabelMouseListener();
		saveButtonListener();
	}

	private void saveButtonListener() {
		SyncNewlyAddedMedia syncMedia = new SyncNewlyAddedMedia();
		saveButton.addActionListener(syncMedia);

	}

	private void setMainPanelBorder() {
		mainBorderPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,
				10));
	}

	public JPanel getBodyPanel() {
		return bodyPanel;
	}

	public void setBodyPanel(JPanel bodyPanel) {
		this.bodyPanel = bodyPanel;
	}

}
