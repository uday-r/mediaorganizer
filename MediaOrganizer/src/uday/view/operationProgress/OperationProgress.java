package uday.view.operationProgress;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;

import uday.service.resourceService.ResourceBundleService;

public class OperationProgress extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JProgressBar progressBar;
	private JButton cancelButton;
	private JPanel mainBorderContainer;
	private JPanel progressBarPanel;
	private JPanel cancelButtonPanel;

	public OperationProgress() throws InterruptedException {
		super(ResourceBundleService.getLabel("copyingFiles"));
		drawFrame();

		setVisible(true);
		setSize(300, 100);
		setLocation(500, 300);

	}

	private void drawFrame() {
		addColorToProgressBar();
		createPanels();
		addLayoutToMainBorderContainer();

		addComponentsToMainBorderContainer();

		mainBorderContainer.setBorder(BorderFactory.createEmptyBorder(10, 10,
				10, 10));

		addLayoutToProgressBarPanel();

		createComponents();
		addComponentsToProgressBarPanel();
		cancelButtonPanel
				.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		bindCancelButton();
		addLayoutToCancelButtonPanel();

		setProgressBar();
		addComponentsToCancelButtonPanel();

		addComponentsToMainFrame();
	}

	private void bindCancelButton() {
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				OperationProgress operationProgress = (OperationProgress) ((JButton) event
						.getSource()).getTopLevelAncestor();
				operationProgress.dispose();
			}
		});
	}

	private void addColorToProgressBar() {
		UIManager.put("ProgressBar.background", Color.white);
		UIManager.put("ProgressBar.foreground", new Color(0x36c0ec));
	}

	private void createPanels() {
		mainBorderContainer = new JPanel();
		progressBarPanel = new JPanel();
		cancelButtonPanel = new JPanel();

	}

	private void addLayoutToMainBorderContainer() {
		mainBorderContainer.setLayout(new BoxLayout(mainBorderContainer,
				BoxLayout.Y_AXIS));

	}

	private void addLayoutToProgressBarPanel() {
		progressBarPanel.setLayout(new BoxLayout(progressBarPanel,
				BoxLayout.Y_AXIS));
	}

	private void addLayoutToCancelButtonPanel() {
		cancelButtonPanel.setLayout(new BoxLayout(cancelButtonPanel,
				BoxLayout.X_AXIS));
	}

	private void createComponents() {
		progressBar = new JProgressBar(0, 100);
		cancelButton = new JButton(ResourceBundleService.getLabel("cancel"));

	}

	private void addComponentsToMainBorderContainer() {
		mainBorderContainer.add(progressBarPanel);
		mainBorderContainer.add(cancelButtonPanel);

	}

	private void addComponentsToProgressBarPanel() {
		progressBarPanel.add(progressBar);

	}

	private void addComponentsToCancelButtonPanel() {
		cancelButtonPanel.add(Box.createHorizontalGlue());
		cancelButtonPanel.add(cancelButton);

	}

	private void setProgressBar() {
		progressBar.setValue(0);
		progressBar.setStringPainted(true);

	}

	private void addComponentsToMainFrame() {
		add(mainBorderContainer);
	}

	public JProgressBar getProgressBar() {
		return this.progressBar;
	}

	public static void main(String[] args) throws InterruptedException {
		OperationProgress operationProgress = new OperationProgress();
		for (int i = 0; i < 1000000; i++) {
			operationProgress.getProgressBar().setValue(i);
		}

	}
}
