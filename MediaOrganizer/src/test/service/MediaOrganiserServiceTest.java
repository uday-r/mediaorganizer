package test.service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.dbunit.DatabaseUnitException;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.model.domainModel.MediaSourceDetails;
import com.model.globalVariables.GlobalVariables;
import com.service.MediaOrganizerDatabaseService;

import exceptions.ConnectionNotAvaliableException;
import exceptions.MediaFileDuplicationException;
import exceptions.MediaFileException;
import exceptions.MediaCopyException;

public class MediaOrganiserServiceTest {

	private static JdbcDatabaseTester databaseTester;
	private static MediaOrganizerDatabaseService movieOrganizerDatabaseService;
	private MediaSourceDetails mediaSourceDetails;
	private FlatXmlDataSet expectedDataSet;
	private ITable expectedMovieTable;
	private IDatabaseConnection connection;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
				"jdbc:mysql://localhost/MediaOrganizer", "root", "8147312585");
		databaseTester.setDataSet(getDataSet());
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	// Test1.
	@Before
	public void setUp() throws Exception {

		databaseTester.onSetup();
		connection = databaseTester.getConnection();
		movieOrganizerDatabaseService = MediaOrganizerDatabaseService
				.getMediaOrganizerInstance();
		mediaSourceDetails = new MediaSourceDetails("Movie", "iron man", false,
				false, true, false, false, "/home/uday_reddy/Movies/iron man", false);
	}

	@Test
	public void testAddNewMedia() {
		try {
			movieOrganizerDatabaseService.insertNewMovie(mediaSourceDetails);
			expectedDataSet = new FlatXmlDataSetBuilder()
					.build(new FileInputStream("MovieListExpected.xml"));

			expectedMovieTable = expectedDataSet.getTable("Movie");

			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualMovieTable = databaseDataSet.getTable("Movie");
			assertEquals(expectedMovieTable.getRowCount(),
					actualMovieTable.getRowCount());
		}catch (SQLException e) {
			fail("SQL exception");
		} catch (IOException e) {
			fail("File IO excepion");
		} catch (InterruptedException e) {
			fail("Interrupted exception");
		} catch (MediaCopyException e) {
			fail("File not copied check if source file is avaliable");
		} catch (MediaFileException e) {
			fail("File already found");
		} catch (DataSetException e) {
			fail("Data-Set exception");
		} catch (ConnectionNotAvaliableException e) {
			fail("Connection not avaliable");
		} catch (MediaFileDuplicationException e) {
			fail("Duplicate Media Found");
		}

	}

	@After
	public void tearDown() throws Exception {
		DatabaseOperation.CLEAN_INSERT.execute(connection,
				new FlatXmlDataSetBuilder().build(new FileInputStream(
						"OriginalData.xml")));

		connection.close();
		FileUtils.deleteDirectory(new File(GlobalVariables
				.getMediaOrganizerPath()
				+ GlobalVariables.getMovieFolder()
				+ mediaSourceDetails.getMediaName()));

		databaseTester.onTearDown();
	}

	// Test2.
	public void test2Setup() {
		try {
			FileUtils
					.moveDirectory(
							new File(GlobalVariables.getMediaOrganizerPath()
									+ GlobalVariables.getMovieFolder()),
							new File(GlobalVariables.getMediaOrganizerPath()
									+ "Movie/"));
		} catch (IOException e) {
			System.out.println("Error while copying");
		}

	}

	@Test
	public void testAddNewMediaNoDestination() {
		test2Setup();
		ITable actualMovieTable = null;
		try {
			movieOrganizerDatabaseService.insertNewMovie(mediaSourceDetails);

			try {
				expectedDataSet = new FlatXmlDataSetBuilder()
						.build(new FileInputStream("AditionalMovie.xml"));
				expectedMovieTable = expectedDataSet.getTable("Movie");

				IDataSet databaseDataSet = connection.createDataSet();
				actualMovieTable = databaseDataSet.getTable("Movie");
				assertEquals(expectedMovieTable.getRowCount(),
						actualMovieTable.getRowCount());
			} catch (DataSetException e1) {
				fail("Data-Set exception");
			} catch (FileNotFoundException e1) {
				fail("File Not Found Exception");
			} catch (SQLException e1) {
				fail("SQL exception");

			}

		}catch (SQLException e) {
			fail("SQL exception");
		} catch (IOException e) {
			fail("File IO excepion");
		} catch (InterruptedException e) {
			fail("Interrupted exception");
		} catch (MediaCopyException e) {
			fail("File Not Found Exception");
		} catch (MediaFileException e) {
			fail("File already found");
		} catch (ConnectionNotAvaliableException e) {
			fail("Connection not avaliable");
		} catch (MediaFileDuplicationException e) {
			fail("Duplicate Media Found");
		}
		test2TearDown();
	}

	public void test2TearDown() {

			try {
				DatabaseOperation.CLEAN_INSERT.execute(connection, getDataSet());
				FileUtils.copyDirectory(
						new File(GlobalVariables.getMediaOrganizerPath()
								+ "Movie/"),
						new File(GlobalVariables.getMediaOrganizerPath()
								+ GlobalVariables.getMovieFolder()));
				FileUtils.deleteDirectory(new File(GlobalVariables.getMediaOrganizerPath()
								+ "Movie/"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}

	// Test3
	public void insertSingleRow() {
		try {
			DatabaseOperation.INSERT.execute(connection,
					new FlatXmlDataSetBuilder().build(new FileInputStream(
							"AddSingleRow.xml")));
		} catch (DatabaseUnitException e) {
			System.err.println("Error in DB unit");
		} catch (SQLException e) {
			System.err.println("Error in Database");
		} catch (Exception e) {
			System.err.println("Error in Application");
		}
	}

	@Test
	public void testForSameMedia() {
		insertSingleRow();
		try {
			movieOrganizerDatabaseService.insertNewMovie(mediaSourceDetails);

		}catch (SQLException e) {
			fail("SQL exception");
		} catch (IOException e) {
			fail("File IO excepion");
		} catch (InterruptedException e) {
			fail("Interrupted exception");
		} catch (MediaCopyException e) {
			fail("File not found exception");
		} catch (MediaFileException e) {
			try {
				expectedDataSet = new FlatXmlDataSetBuilder()
						.build(new FileInputStream("AditionalMovie.xml"));

				expectedMovieTable = expectedDataSet.getTable("Movie");

				IDataSet databaseDataSet = connection.createDataSet();
				ITable actualMovieTable = databaseDataSet.getTable("Movie");
				assertEquals(expectedMovieTable.getRowCount(),
						actualMovieTable.getRowCount());

			} catch (DataSetException e1) {
				fail("Data-Set exception");
			} catch (FileNotFoundException e1) {
				fail("File Not Found Exception");
			} catch (SQLException e1) {
				fail("SQL exception");

			}
		} catch (ConnectionNotAvaliableException e) {
			fail("Connection not avaliable");
		} catch (MediaFileDuplicationException e) {
			fail("Duplicate Media Found");
		}

	}

	// Test4.
		public void test4Setup() {
			try {
				FileUtils.copyDirectory(new File("/home/uday_reddy/Movies/iron man"),
								new File("/home/uday_reddy/Movies/iron"));
				FileUtils.deleteDirectory(new File("/home/uday_reddy/Movies/iron man"));
			} catch (IOException e) {
				System.out.println("Error while copying");
			}

		}

		@Test
		public void testAddNewMediaNoSource() {
			test2Setup();
			ITable actualMovieTable = null;
			try {
				movieOrganizerDatabaseService.insertNewMovie(mediaSourceDetails);

				
			}catch (SQLException e) {
				fail("SQL exception");
			} catch (IOException e) {
				fail("File IO excepion");
			} catch (InterruptedException e) {
				fail("Interrupted exception");
			} catch (MediaCopyException e) {
				fail("File Not Found Exception");
			} catch (MediaFileException e) {
				try {
					expectedDataSet = new FlatXmlDataSetBuilder()
							.build(new FileInputStream("AditionalMovie.xml"));
					expectedMovieTable = expectedDataSet.getTable("Movie");

					IDataSet databaseDataSet = connection.createDataSet();
					actualMovieTable = databaseDataSet.getTable("Movie");
					assertEquals(expectedMovieTable.getRowCount(),
							actualMovieTable.getRowCount());
				} catch (DataSetException e1) {
					fail("Data-Set exception");
				} catch (FileNotFoundException e1) {
					fail("File Not Found Exception");
				} catch (SQLException e1) {
					fail("SQL exception");

				}

			} catch (ConnectionNotAvaliableException e) {
				fail("Connection not avaliable");
			} catch (MediaFileDuplicationException e) {
				fail("Duplicate Media Found");
			}
			test2TearDown();
		}

		public void test4TearDown() {

				try {
					DatabaseOperation.CLEAN_INSERT.execute(connection, getDataSet());
					FileUtils.copyDirectory(new File("/home/uday_reddy/Movies/iron"),
							new File("/home/uday_reddy/Movies/iron man"));
					FileUtils.deleteDirectory(new File("/home/uday_reddy/Movies/iron"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}

	protected static IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream(
				"OriginalData.xml"));
	}

}
