package test.service;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.model.domainModel.MediaSourceDetails;
import com.service.MediaOrganizerDatabaseService;

public class TestSample {

	private static JdbcDatabaseTester databaseTester;
	private static MediaOrganizerDatabaseService movieOrganizerDatabaseService;
	private static ArrayList<MediaSourceDetails> movieSourceDetailsList;
	private FlatXmlDataSet expectedDataSet;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
				"jdbc:mysql://localhost/MediaOrganizer", "root", "8147312585");
		databaseTester.setDataSet(getDataSet());
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Before
	public void setUp() throws Exception {
		

		databaseTester.onSetup();

		movieOrganizerDatabaseService = MediaOrganizerDatabaseService
				.getMediaOrganizerInstance();
		MediaSourceDetails movieSourceDetails = new MediaSourceDetails("Movie",
				"iron man", true, false, true, false, false,
				"/home/uday_reddy/MediaOrganizer/Movie/iron man/");
		movieSourceDetailsList = new ArrayList<MediaSourceDetails>();
		movieSourceDetailsList.add(movieSourceDetails);
	}

	@After
	public void tearDown() throws Exception {
		databaseTester.onTearDown();
	}

	@Test
	public void test() {
		try {
			movieOrganizerDatabaseService
					.insertNewMedia(movieSourceDetailsList);
			expectedDataSet = new FlatXmlDataSetBuilder()
					.build(new FileInputStream("MovieListExpected.xml"));
			
			ITable expectedMovieTable = expectedDataSet.getTable("Movie");

			IDatabaseConnection connection = databaseTester.getConnection();
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualMovieTable = databaseDataSet.getTable("Movie");
			assertEquals(expectedMovieTable.getRowCount(),
					actualMovieTable.getRowCount());
			DatabaseOperation.CLEAN_INSERT.execute(connection, getDataSet());

		} catch (DataSetException e) {
			e.printStackTrace();
			System.err.println("Sorry, Error in the Data Set." + e.getMessage()
					+ "\n");
		} catch (FileNotFoundException e) {
			System.err.println("Sorry, File Not Found. " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Sorry, Error in Database please try again later.\n"
							+ e.getMessage());
		}
	}

	protected static IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder()
		.build(new FileInputStream("Original.xml"));
	}

}
