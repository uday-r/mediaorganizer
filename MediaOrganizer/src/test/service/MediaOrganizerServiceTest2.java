package test.service;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.model.domainModel.MediaSourceDetails;
import com.service.MediaOrganizerDatabaseService;

public class MediaOrganizerServiceTest2 extends DBTestCase {

	private static IDatabaseTester databaseTester;
	private ArrayList<MediaSourceDetails> movieSourceDetailsList;
	private MediaOrganizerDatabaseService movieOrganizerDatabaseService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("The database is initialized");
		databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
				"jdbc:mysql://localhost/MediaOrganizer", "root", "8147312585");
//		IDataSet dataSet = new FlatXmlDataSetBuilder()
//				.build(new FileInputStream("MovieListInitial.xml"));
//		databaseTester.setDataSet(dataSet);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		System.out.println("The database is initialized");
		databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
				"jdbc:mysql://localhost/MediaOrganizer", "root", "8147312585");
		IDataSet dataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("Original.xml"));
		databaseTester.setDataSet(dataSet);
		
		movieOrganizerDatabaseService = MediaOrganizerDatabaseService
				.getMediaOrganizerInstance();
		MediaSourceDetails movieSourceDetails = new MediaSourceDetails("Movie",
				"iron man", true, false, true, false, false,
				"/home/uday_reddy/movies/iron man/");
		movieSourceDetailsList = new ArrayList<MediaSourceDetails>();
		movieSourceDetailsList.add(movieSourceDetails);

		 databaseTester.onSetup();
	}

	@After
	public void tearDown() throws Exception {
		databaseTester.onTearDown();
	}

//	@Test
//	public void testAddNewMovie() throws SQLException, InstantiationException,
//			IllegalAccessException, ClassNotFoundException {
//
//		IDataSet expectedDataSet;
//		try {
//
//			movieOrganizerDatabaseService
//					.insertNewMedia(movieSourceDetailsList);
//			expectedDataSet = new FlatXmlDataSetBuilder()
//					.build(new FileInputStream("MovieListExpected.xml"));
//			ITable expectedMovieTable = expectedDataSet.getTable("Movie");
//
//			IDatabaseConnection connection = databaseTester.getConnection();
//			IDataSet databaseDataSet = connection.createDataSet();
//			ITable actualMovieTable = databaseDataSet.getTable("Movie");
//			assertEquals(expectedMovieTable.getRowCount(),
//					actualMovieTable.getRowCount());
//
//		} catch (DataSetException e) {
//			e.printStackTrace();
//			System.err.println("Sorry, Error in the Data Set." + e.getMessage()
//					+ "\n");
//		} catch (FileNotFoundException e) {
//			System.err.println("Sorry, File Not Found. " + e.getMessage());
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.err
//					.println("Sorry, Error in Database please try again later.\n"
//							+ e.getMessage());
//		}
//
//	}

	@Test
	public void testDeleteMedia() {
		IDataSet expectedDataSet;

		try {
			movieOrganizerDatabaseService
					.insertNewMedia(movieSourceDetailsList);
			expectedDataSet = new FlatXmlDataSetBuilder()
					.build(new FileInputStream("Original.xml"));
			ITable expectedMovieTable = expectedDataSet.getTable("Movie");
			
			movieOrganizerDatabaseService.deleteMedia(movieSourceDetailsList);
			
			IDatabaseConnection connection = databaseTester.getConnection();
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualMovieTable = databaseDataSet.getTable("Movie");
			System.out.println("Expected: "+expectedMovieTable.getRowCount()+"\n Actual: "+actualMovieTable.getRowCount());
			assertEquals(expectedMovieTable.getRowCount(),actualMovieTable.getRowCount());
			
		} catch (DataSetException e) {
			e.printStackTrace();
			System.err.println("Sorry, Error in the Data Set." + e.getMessage()
					+ "\n");
		} catch (FileNotFoundException e) {
			System.err.println("Sorry, File Not Found. " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.err
					.println("Sorry, Error in Database please try again later.\n"
							+ e.getMessage());
		}

	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		QueryDataSet queryDataSet = null;
		String query = "SELECT * FROM Movie where Movie_Name = ?";
		try {
			queryDataSet = new QueryDataSet(super.getConnection());
			queryDataSet.addTable("Movie", query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return queryDataSet;
	}

	// @Override
	// protected DatabaseOperation getSetUpOperation() throws Exception
	// {
	// return DatabaseOperation.CLEAN_INSERT; // by default (will do DELETE_ALL
	// + INSERT)
	// }
	//
	// @Override
	// protected DatabaseOperation getTearDownOperation() throws Exception
	// {
	// return DatabaseOperation.NONE; // by default
	// }
}
