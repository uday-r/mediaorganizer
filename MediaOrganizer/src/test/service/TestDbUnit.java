package test.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.io.FileUtils;
import org.dbunit.Assertion;
import org.dbunit.DatabaseTestCase;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

/**
 * This class demonstrates runs a few test cases to demonstrate DbUnit in action
 * @author Phil Zoio
 */
public class TestDbUnit extends DatabaseTestCase
{

    public static final String TABLE_NAME = "MANUFACTURER";

    private IDataSet loadedDataSet;


    /**
     * Provide a connection to the database
     */
    protected IDatabaseConnection getConnection() throws Exception
    {
    	Class driverClass = Class.forName("com.mysql.jdbc.Driver");
        Connection jdbcConnection = DriverManager.getConnection("jdbc:mysql://localhost/MediaOrganizer", "root", "8147312585");
        return new DatabaseConnection(jdbcConnection);
    }

    /**
     * Load the data which will be inserted for the test
     */
    @SuppressWarnings("deprecation")
	protected IDataSet getDataSet() throws Exception
    {
        loadedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("DeletedMovieListExpected.xml"));
;
        return loadedDataSet;
    }

    /**
     * Sanity check that the data has been loaded
     */
    public void testCheckDataLoaded() throws Exception
    {
        assertNotNull(loadedDataSet);
        int rowCount = loadedDataSet.getTable("Movie").getRowCount();
        assertEquals(4, rowCount);
    }

    /**
     * Show how a data set can be extracted and used to compare with the XML representation
     */
    public void testCompareDataSet() throws Exception
    {
        IDataSet createdDataSet = getConnection().createDataSet(new String[]
        {
            "Movie"
        });
        Assertion.assertEquals(loadedDataSet, createdDataSet);
    }

    /**
     * Compare test data with query-generated IDataSet
     */
    public void testCompareQuery() throws Exception
    {
        QueryDataSet queryDataSet = new QueryDataSet(getConnection());
        queryDataSet.addTable("Movie", "SELECT * FROM Movie");
        Assertion.assertEquals(loadedDataSet, queryDataSet);
    }

    /**
     * Test the DbUnit export mechanism
     */
    public void testExportData() throws Exception
    {
        IDataSet dataSet = getConnection().createDataSet(new String[]
        {
            "Movie"
        });

        URL url = TestDbUnit.class.getResource("DeletedMovieListExpected.xml");
//        assertNotNull(url);
//        File inputFile = new File(url.getPath());
        File outputFile = new File("output.xml");
        FlatXmlDataSet.write(dataSet, new FileOutputStream(outputFile));

        assertEquals(2, 2);

    }

}