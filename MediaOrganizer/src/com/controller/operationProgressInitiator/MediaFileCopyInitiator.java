package com.controller.operationProgressInitiator;

import java.io.File;

import com.Utils.MediaCopy;
import com.model.domainModel.FileCopyStatus;
import com.model.domainModel.MediaSourceDetails;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class MediaFileCopyInitiator {

	private MediaSourceDetails mediaSourceDetails;
	
	public MediaFileCopyInitiator(MediaSourceDetails mediaSourceDetails) throws InterruptedException {
		this.mediaSourceDetails = mediaSourceDetails;
	}


	public FileCopyStatus iterateFilesForProgressUpdate(String sourceFilePath,
			String destinationFilePath) throws InterruptedException {
		File sourceFile = new File(sourceFilePath);
		long mediaSourceSize, mediaDestinationSize;
		if (!sourceFile.isDirectory()) {
			destinationFilePath = destinationFilePath + FilenameUtils.removeExtension(sourceFile.getName());
			File destinationFile = new File(destinationFilePath);
			destinationFile.mkdir();
			mediaSourceSize = FileUtils.sizeOf(sourceFile);
		} else {
			mediaSourceSize = FileUtils.sizeOfDirectory(sourceFile);
		}
		
		MediaCopy.initiateCopy(sourceFilePath, destinationFilePath, mediaSourceDetails);
		
		File copiedDestinationFile = new File(destinationFilePath + "/" + sourceFile.getName());
		mediaDestinationSize = FileUtils.sizeOfDirectory(copiedDestinationFile);

		

		return getFileCopyStatus(mediaDestinationSize, mediaSourceSize);
	}

	private FileCopyStatus getFileCopyStatus(long mediaDestinationSize, long mediaSourceSize){
		FileCopyStatus fileCopyStatus; 
		if (mediaDestinationSize == mediaSourceSize) {
			fileCopyStatus = new FileCopyStatus("Success", "File Copied");
		} else {
			fileCopyStatus = new FileCopyStatus("failure", "File not Copied");
		}
		return fileCopyStatus;
	}


	public MediaSourceDetails getMediaSourceDetails() {
		return mediaSourceDetails;
	}


	public void setMediaSourceDetails(MediaSourceDetails mediaSourceDetails) {
		this.mediaSourceDetails = mediaSourceDetails;
	}
	
}
