package com.controller.editMedia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.view.mediaOrganizer.Button;
import com.view.editMediaDetails.MediaFileMissing;

public class MediaMissingAction implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent event) {
		Button button = (Button)event.getSource();
		MediaFileMissing mediaFileMissing = (MediaFileMissing) button.getTopLevelAncestor();
		mediaFileMissing.dispose();
	}

}
