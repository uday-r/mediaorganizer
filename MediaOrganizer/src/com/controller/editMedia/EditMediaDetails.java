package com.controller.editMedia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.Utils.ErrorMessage;
import com.model.domainModel.TableRowData;
import com.view.mediaOrganizer.MediaOrganizerView;
import com.view.mediaOrganizer.ResultSetTable;
import com.view.mediaOrganizer.RowSelectionEditButton;

public class EditMediaDetails implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent event) {

		RowSelectionEditButton buttonObject = (RowSelectionEditButton) event.getSource();
		MediaOrganizerView mediaOrganizerView = (MediaOrganizerView) buttonObject.getTopLevelAncestor();
		String mediaType = mediaOrganizerView.getMediaType();
		ResultSetTable resultSetTable = mediaOrganizerView.getresultSetTable();
		int selectedRow = resultSetTable.getSelectedRow();
		TableRowData tableRowData = new TableRowData(resultSetTable, selectedRow);
		System.out.println("Class Name: "+"com.view.editMediaDetails.Edit"+mediaType+"DetailsView");
		try {
			Class editMedia = Class.forName("com.view.editMediaDetails.Edit"+mediaType+"DetailsView");
			Constructor constructor = editMedia.getConstructor(TableRowData.class);
			try {
				constructor.newInstance(tableRowData);
			} catch (IllegalArgumentException e){
				ErrorMessage.getErrorMessage();
			} catch (InvocationTargetException e) {
				ErrorMessage.getErrorMessage();
			}
		} catch (ClassNotFoundException e) {
			ErrorMessage.getErrorMessage();
		} catch (InstantiationException e) {
			ErrorMessage.getErrorMessage();
		} catch (IllegalAccessException e) {
			ErrorMessage.getErrorMessage();
		} catch (NoSuchMethodException e) {
			ErrorMessage.getErrorMessage();
		} catch (SecurityException e) {
			ErrorMessage.getErrorMessage();
		}
	}

}
