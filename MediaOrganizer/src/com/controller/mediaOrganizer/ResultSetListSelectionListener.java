package com.controller.mediaOrganizer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import com.view.mediaOrganizer.ResultSetTable;
import com.view.mediaOrganizer.RowSelectionButton;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ResultSetListSelectionListener extends Observable implements
		ListSelectionListener {

	ArrayList<RowSelectionButton> observers = new ArrayList<RowSelectionButton>();
	ArrayList<Boolean> buttonEnableStatus = new ArrayList<Boolean>();
	ResultSetTable table;
	
	public ResultSetListSelectionListener(ResultSetTable table){
		this.table = table;
		buttonEnableStatus.add(false);
		buttonEnableStatus.add(false);
	}
	
	@Override
	public void addObserver(Observer observer) {
		super.addObserver(observer);
		observers.add((RowSelectionButton) observer);
	}

	public void valueChanged(ListSelectionEvent event) {

		if (!event.getValueIsAdjusting()) {
			int selectedRowCount = table.getSelectedRowCount();
			int selectedRow = table.getSelectedRow();
			if (selectedRowCount >1 ) {

				System.out.println("Disable edit button");
				
				buttonEnableStatus.set(1, true);
				buttonEnableStatus.set(0, false);
				
			}
			else{
				buttonEnableStatus.set(0, true);
				buttonEnableStatus.set(1, true);
				System.out.println(selectedRow);
				setChanged();
				notifyObservers(buttonEnableStatus);	
			}
			
			
			if(selectedRowCount == 0){
				buttonEnableStatus.set(0, false);
				buttonEnableStatus.set(1, false);
				setChanged();
				notifyObservers(buttonEnableStatus);	
				
			}
			else{
				setChanged();
				notifyObservers(buttonEnableStatus);	
			}
			
		}
	}

}

