package com.controller.mediaOrganizer;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.view.mediaOrganizer.MediaOrganizerView;

public class MediaOrganizerController {
	
	static Logger log = Logger.getLogger(MediaOrganizerController.class.getName());
	public static void main(String[] args){
		PropertyConfigurator.configure("Log4j.properties");
		log.info(MediaOrganizerController.class.getName()+" Initiated.");
		new MediaOrganizerView(); 
	}
}
