package com.controller.mediaOrganizer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.apache.log4j.Logger;

import com.view.addNewMedia.AddNewMediaView;

public class AddMediaButtonActionListener implements ActionListener {

	static Logger log = Logger.getLogger(AddMediaButtonActionListener.class.getName());
	@Override
	public void actionPerformed(ActionEvent event) {
		log.info("Action performed ");
		new AddNewMediaView();
	}

}
