package com.controller.mediaOrganizer;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import com.view.mediaOrganizer.SearchTextField;

public class SearchByTextBoxFocusListener implements FocusListener{
	private String  getTextBoxContents(FocusEvent event){
		return ((SearchTextField)event.getSource()).getText();
	}
	private void setNullToTextBoxContents(FocusEvent event){
		((SearchTextField)event.getSource()).setText(null);
	}
	@Override
	public void focusGained(FocusEvent event) {
		String searchBoxText = getTextBoxContents(event);
		if(searchBoxText.matches("Start Typing the.*")){
			setNullToTextBoxContents(event);
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}
}
