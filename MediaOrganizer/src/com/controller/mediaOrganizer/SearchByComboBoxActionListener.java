package com.controller.mediaOrganizer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import com.model.domainModel.MediaTypeDetails;
import com.view.mediaOrganizer.MediaOrganizerView;
import com.view.mediaOrganizer.SearchByComboBox;

public class SearchByComboBoxActionListener extends Observable implements
		ActionListener {

	@Override
	public void actionPerformed(ActionEvent event) {

		SearchByComboBox searchByComboBox = (SearchByComboBox) event
				.getSource();
		MediaOrganizerView mediaOrganizerView = (MediaOrganizerView) searchByComboBox
				.getTopLevelAncestor();
		String mediaType = mediaOrganizerView.getMediaType();

		String mediaProperty = (String) searchByComboBoxSelectedItem(event);
		MediaTypeDetails mediaTypeDetails = new MediaTypeDetails(mediaType,
				mediaProperty, null);
		setChanged();
		notifyObservers(mediaTypeDetails);
	}

	private Object searchByComboBoxSelectedItem(ActionEvent event) {
		Object defaultText = null;
		if (((SearchByComboBox) event.getSource()).getSelectedItem() == "Search By") {
			defaultText = "Media Name";
		} else {
			defaultText = ((SearchByComboBox) event.getSource())
					.getSelectedItem();
		}

		return defaultText;
	}

}
