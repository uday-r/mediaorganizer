package com.controller.mediaOrganiser;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import com.service.MediaOrganiserDatabaseService;
import com.view.mediaOrganiser.MediaOrganiserView;
import com.view.mediaOrganiser.SearchByComboBox;
import com.view.mediaOrganiser.SearchTextField;
import com.model.domainModel.MediaFilter;
import com.model.domainModel.MediaTypeDetails;

public class SearchByTextBoxKeyListener extends Observable implements KeyListener {

	private MediaOrganiserDatabaseService getDatabaseInstance()
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, SQLException {
		MediaOrganiserDatabaseService mediaOrganiserDatabaseService = MediaOrganiserDatabaseService
				.getMediaOrganiserInstance();
		return mediaOrganiserDatabaseService;
	}

	@Override
	public void keyPressed(KeyEvent event) {
		

	}
	@Override
	public void keyTyped(KeyEvent event) {

	}
	
	
	@Override
	public void keyReleased(KeyEvent event) {
		try {
			MediaOrganiserDatabaseService mediaOrganiserDatabaseService = getDatabaseInstance();
			
			SearchTextField searchByTextField = (SearchTextField) event.getSource();
			
			
			MediaOrganiserView mediaOrganiserView = (MediaOrganiserView)searchByTextField.getTopLevelAncestor();
			String media = searchByTextField.getText();
			String mediaType = mediaOrganiserView.getMediaType();
			String mediaTypeProperty = mediaOrganiserView.getMediaTypeProperty();
			MediaTypeDetails mediaTypeDetails = new MediaTypeDetails(mediaType, mediaTypeProperty, media);
			ArrayList<ArrayList<String>> searchResults;
			searchResults = mediaOrganiserDatabaseService.getSearchedMedia(mediaTypeDetails);
			MediaFilter mediaFilter = new MediaFilter(mediaTypeDetails, searchResults);
			setChanged();
			notifyObservers(mediaFilter);

		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
	}

	
}
