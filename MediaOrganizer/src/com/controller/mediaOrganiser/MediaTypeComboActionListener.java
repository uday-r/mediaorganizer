package com.controller.mediaOrganiser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.table.TableModel;

import com.model.domainModel.MediaFilter;
import com.model.domainModel.MediaTypeDetails;
import com.model.viewModel.ResultSetTableModel;
import com.service.MediaOrganiserDatabaseService;
import com.view.mediaOrganiser.MediaOrganiserView;
import com.view.mediaOrganiser.MediaTypeComboBox;
import com.view.mediaOrganiser.ResultSetTable;

public class MediaTypeComboActionListener extends Observable implements
		ActionListener {

	@Override
	public void actionPerformed(ActionEvent event) {

		MediaTypeComboBox mediaTypeComboBox = (MediaTypeComboBox) event
				.getSource();
		
		
		MediaOrganiserView mediaOrganiserView = (MediaOrganiserView)mediaTypeComboBox.getTopLevelAncestor();
		String media = null;
		String mediaType = (String) mediaTypeComboBox.getSelectedItem();
		String mediaTypeProperty = null;
		MediaTypeDetails mediaTypeDetails = new MediaTypeDetails(mediaType, mediaTypeProperty, media);
		ArrayList<ArrayList<String>> searchResults = null;
		MediaOrganiserDatabaseService mediaOrganiserDatabaseService;
		try {
			mediaOrganiserDatabaseService = MediaOrganiserDatabaseService.getMediaOrganiserInstance();
			searchResults = mediaOrganiserDatabaseService.getSearchedMedia(mediaTypeDetails);
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MediaFilter mediaFilter = new MediaFilter(mediaTypeDetails, searchResults);
		
		
		
		setChanged();
		notifyObservers(mediaFilter);
	}

}
