package com.controller.mediaOrganiser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.view.addNewMedia.AddNewMediaView;
import com.view.mediaOrganiser.AddMediaComboBox;

public class AddMediaComboBoxActionListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent event) {
		AddNewMediaView addNewMediaController = new AddNewMediaView(getSelectedMediaType(event));
	}

	private String getSelectedMediaType(ActionEvent event){
		return getMediaType(((AddMediaComboBox) event.getSource()).getSelectedItem().toString());
		
	}
	
	private String getMediaType(String selectedMediaType){
		return selectedMediaType.split(" ")[1];
	}

}
