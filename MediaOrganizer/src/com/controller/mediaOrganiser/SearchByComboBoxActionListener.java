package com.controller.mediaOrganiser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import com.model.domainModel.MediaTypeDetails;
import com.view.mediaOrganiser.MediaOrganiserView;
import com.view.mediaOrganiser.MediaTypeComboBox;
import com.view.mediaOrganiser.SearchByComboBox;

public class SearchByComboBoxActionListener extends Observable implements
		ActionListener {

	@Override
	public void actionPerformed(ActionEvent event) {

		SearchByComboBox searchByComboBox = (SearchByComboBox) event
				.getSource();
		MediaOrganiserView mediaOrganiserView = (MediaOrganiserView) searchByComboBox
				.getTopLevelAncestor();
		String mediaType = mediaOrganiserView.getMediaType();

		String mediaProperty = (String) searchByComboBoxSelectedItem(event);
		MediaTypeDetails mediaTypeDetails = new MediaTypeDetails(mediaType,
				mediaProperty, null);
		setChanged();
		notifyObservers(mediaTypeDetails);
	}

	private Object searchByComboBoxSelectedItem(ActionEvent event) {
		Object defaultText = null;
		if (((SearchByComboBox) event.getSource()).getSelectedItem() == "Search By") {
			defaultText = "Media Name";
		} else {
			defaultText = ((SearchByComboBox) event.getSource())
					.getSelectedItem();
		}

		return defaultText;
	}

}
