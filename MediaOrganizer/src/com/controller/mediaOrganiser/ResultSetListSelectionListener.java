package com.controller.mediaOrganiser;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import com.model.domainModel.TableRowData;
import com.model.domainModel.TableRowSelectionResponse;
import com.view.mediaOrganiser.ResultSetTable;
import com.view.mediaOrganiser.RowSelectionButton;
import com.view.mediaOrganiser.RowSelectionEditButton;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ResultSetListSelectionListener extends Observable implements
		ListSelectionListener {

	ArrayList<RowSelectionButton> observers = new ArrayList<RowSelectionButton>();
	ArrayList<Boolean> buttonEnableStatus = new ArrayList<Boolean>();
	ResultSetTable table;
	
	public ResultSetListSelectionListener(ResultSetTable table){
		this.table = table;
		buttonEnableStatus.add(false);
		buttonEnableStatus.add(false);
	}
	
	@Override
	public void addObserver(Observer observer) {
		super.addObserver(observer);
		observers.add((RowSelectionButton) observer);
	}

	public void valueChanged(ListSelectionEvent event) {

		if (!event.getValueIsAdjusting()) {
			ListSelectionModel lsm = (ListSelectionModel) event.getSource();
			int selectedRowCount = table.getSelectedRowCount();
			int selectedRow = table.getSelectedRow();
			TableRowData tableRowData = null;
			TableRowSelectionResponse responseTableData = null;
			if (selectedRowCount >1 ) {

				System.out.println("Disable edit button");
				
				buttonEnableStatus.set(1, true);
				buttonEnableStatus.set(0, false);
				
			}
			else{
				buttonEnableStatus.set(0, true);
				buttonEnableStatus.set(1, true);
				System.out.println(selectedRow);
				setChanged();
				notifyObservers(buttonEnableStatus);	
			}
			
			
			if(selectedRowCount == 0){
				buttonEnableStatus.set(0, false);
				buttonEnableStatus.set(1, false);
				setChanged();
				notifyObservers(buttonEnableStatus);	
				
			}
			else{
				setChanged();
				notifyObservers(buttonEnableStatus);	
			}
			
		}
	}

}

