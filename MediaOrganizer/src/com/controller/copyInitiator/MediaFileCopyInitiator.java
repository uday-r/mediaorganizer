package com.controller.copyInitiator;

import java.io.File;
import java.io.IOException;

import com.Utils.fileOperations.MediaCopy;
import com.model.domainModel.MediaSourceDetails;
import com.model.globalVariables.GlobalVariables;

import exceptions.MediaCopyException;
import exceptions.MediaFileException;

import org.apache.commons.io.FilenameUtils;

public class MediaFileCopyInitiator {

	private MediaSourceDetails mediaSourceDetails;
	
	public MediaFileCopyInitiator(MediaSourceDetails mediaSourceDetails) throws InterruptedException {
		this.mediaSourceDetails = mediaSourceDetails;
	}


	public void prepareForCopy(String sourceFilePath,
			String destinationFilePath) throws  MediaCopyException,  IOException, MediaFileException{
		File sourceFile = null;
		sourceFile = new File(sourceFilePath);
		if(!sourceFile.exists()){
			throw new MediaFileException("Source Media File Not Found");
		}

		if(!new File(GlobalVariables.getMediaOrganizerPath()+GlobalVariables.getMovieFolder()).exists()){
			new File(GlobalVariables.getMediaOrganizerPath()+GlobalVariables.getMovieFolder()).mkdirs();
		}
		if (!sourceFile.isDirectory()) {
			destinationFilePath = destinationFilePath + FilenameUtils.removeExtension(sourceFile.getName());
			File destinationFile = new File(destinationFilePath);
			destinationFile.mkdir();
		}

		
		MediaCopy.startCopy(sourceFilePath, destinationFilePath, mediaSourceDetails);
			
	
		int sourceFileSelectedToCopy = 0;
		if(mediaSourceDetails.getAudioFile()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.getVideoFile()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.getPoster()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.getSubtitle()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(mediaSourceDetails.geteBookFile()){
			sourceFileSelectedToCopy = sourceFileSelectedToCopy + 1;
		}
		if(sourceFileSelectedToCopy != MediaCopy.getFilesCopied()){
			throw new MediaCopyException("Encountered some internal problem , file not copied");
		}
	}

	

    
	public MediaSourceDetails getMediaSourceDetails() {
		return mediaSourceDetails;
	}


	public void setMediaSourceDetails(MediaSourceDetails mediaSourceDetails) {
		this.mediaSourceDetails = mediaSourceDetails;
	}
	
}
