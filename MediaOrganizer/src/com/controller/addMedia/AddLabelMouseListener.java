package com.controller.addMedia;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.apache.commons.io.FilenameUtils;

import com.model.globalVariables.GlobalVariables;
import com.model.viewModel.AddMediaFilesTableModel;
import com.view.addNewMedia.AddMediaFilesTable;

import ebook.EBook;
import ebook.parser.InstantParser;
import ebook.parser.Parser;
import eu.medsea.mimeutil.MimeUtil2;

public class AddLabelMouseListener implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent event) {
		AddMediaFilesTableModel tableModel = getAddMediaFilesTableModel(event);

		JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(true);

		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fc.showOpenDialog(null);

		File[] files = fc.getSelectedFiles();

		for (int i = 0; i < files.length; i++) {

			String path = files[i].getPath();
			File mediaFile = new File(path);

			tableModel.addRow(mediaSourceDetailsFactory(mediaFile));

		}

	}

	private Object[] mediaSourceDetailsFactory(File mediaFile) {

		String mediaType = "none";

		Boolean poster = new Boolean(false);
		Boolean subtitle = new Boolean(false);
		Boolean audio = new Boolean(false);
		Boolean video = new Boolean(false);
		Boolean book = new Boolean(false);
		int maxFileCount;
		File[] filesList = mediaFile.listFiles();
		if (filesList == null) {
			maxFileCount = 0;
		} else {
			maxFileCount = mediaFile.listFiles().length;
		}

		for (int fileCount = 0; fileCount < maxFileCount; fileCount++) {
			File file;
			if (filesList == null) {
				file = mediaFile;
			} else {
				file = mediaFile.listFiles()[fileCount];
			}

			String path = file.getAbsolutePath();
			MimeUtil2 mimeUtil = new MimeUtil2();
			mimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
			String mediaFileType = MimeUtil2
					.getMostSpecificMimeType(mimeUtil.getMimeTypes(file))
					.toString().split("/")[0];

			if (mediaFileType.equals("image")) {
				poster = true;
			}
			String mediaFileExtension = FilenameUtils.getExtension(path);
			if (mediaFileExtension.equals("srt")) {
				subtitle = true;
			}
			if (mediaFileType.equals("audio")) {
				audio = true;
				if (mediaType.equals("none")) {
					mediaType = GlobalVariables.getMusicAsMediaType();
				}
			}
			if (mediaFileType.equals("video")) {
				video = true;
				mediaType = GlobalVariables.getMovieAsMediaType();
			}
			Parser parser = new InstantParser();
			EBook ebook = parser.parse(path);
			if (ebook.isOk) {
				book = true;
				if (mediaType.equals("none")) {
					mediaType = GlobalVariables.getBookAsMediaType();
				}
			}

		}

		Object[] row = new Object[] { mediaType, mediaFile.getName(), poster,
				subtitle, video, audio, book, mediaFile.getPath(),
				null ,new Boolean(false) };
		return row;
	}

	private AddMediaFilesTableModel getAddMediaFilesTableModel(MouseEvent event) {
		return (AddMediaFilesTableModel) ((AddMediaFilesTable) ((JScrollPane) ((JLabel) event
				.getSource()).getParent().getParent().getComponents()[1])
				.getViewport().getComponents()[0]).getModel();
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

}
