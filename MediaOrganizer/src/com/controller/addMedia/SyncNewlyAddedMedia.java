package com.controller.addMedia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import com.Utils.ErrorMessage;
import com.Utils.inputValidation.MediaInputValidate;
import com.model.domainModel.FileCopyStatus;
import com.model.domainModel.MediaSourceDetails;
import com.model.viewModel.AddMediaFilesTableModel;
import com.service.MediaOrganizerDatabaseService;
import com.view.addNewMedia.AddMediaFilesTable;
import com.view.addNewMedia.AddNewMediaView;
import com.view.operationProgress.OperationProgress;

import exceptions.ConnectionNotAvaliableException;
import exceptions.MediaCopyException;
import exceptions.IncorrectInput;
import exceptions.MediaFileDuplicationException;
import exceptions.MediaFileException;

public class SyncNewlyAddedMedia implements ActionListener {


	@Override
	public void actionPerformed(ActionEvent event) {
		Boolean dispose = false;
		AddMediaFilesTableModel addedMediaFiles = getAddMediaFilesTableModel(event);
		AddMediaFilesTable addMediaFilesTable = getAddMediaFilesTable(event);

		ArrayList<MediaSourceDetails> mediaSourceDetailsList = new ArrayList<MediaSourceDetails>();

		for (int i = 0; i < addedMediaFiles.getRowCount(); i++) {

			ArrayList<String> row = new ArrayList<String>();

			row.add((String) addedMediaFiles.getValueAt(i, 0));
			row.add((String) addedMediaFiles.getValueAt(i, 1));
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 2)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 3)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 4)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 5)).toString());
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 6)).toString());
			row.add((String) addedMediaFiles.getValueAt(i, 7));
			row.add((String) new Boolean((Boolean) addedMediaFiles.getValueAt(
					i, 9)).toString());
			mediaSourceDetailsList.add(new MediaSourceDetails(row));
		}
		try {

			MediaOrganizerDatabaseService databaseServiceInstance = MediaOrganizerDatabaseService
					.getMediaOrganizerInstance();

			OperationProgress operationProgress = null;
			JProgressBar progressBar = null;
			Boolean initiateCopy = true;
			if (!mediaSourceDetailsList.get(0).getCopyStatus()
					.equals("Failure:File Already found")) {
				operationProgress = new OperationProgress();
				progressBar = operationProgress.getProgressBar();
				progressBar.setValue(0);
				initiateCopy = true;
			}

			int fileCount = 0;

			ArrayList<Integer> removeMediaSourceDetailsIndex = new ArrayList<Integer>();

			for (MediaSourceDetails mediaSourceDetails : mediaSourceDetailsList) {
				System.out.println("Overwrite : "
						+ mediaSourceDetails.getOverwriteMedia());
				try {
					MediaInputValidate.getMediaType(mediaSourceDetails);

					if (mediaSourceDetails.getMediaType().equals("Movie")) {
						try {
							databaseServiceInstance
									.insertNewMovie(mediaSourceDetails);
							removeMediaSourceDetailsIndex
									.add(mediaSourceDetailsList
											.indexOf(mediaSourceDetails));
							dispose = true;
						} catch (SQLException e) {
							ErrorMessage.getDataBaseErrorMessage(e.getMessage());
							mediaSourceDetails = setErrorMessage(event, mediaSourceDetailsList,
									mediaSourceDetails, new FileCopyStatus(
											"Error", "Error in DataBase"), e);

						} catch (MediaCopyException e) {
							ErrorMessage.getFileNotCopied();
							mediaSourceDetails = setErrorMessage(event, mediaSourceDetailsList,
									mediaSourceDetails, new FileCopyStatus("Error",
											"File not copied"), e);
						} catch (MediaFileDuplicationException e) {
							addMediaFilesTable.hideFolderDetailColumns();
							mediaSourceDetails = setErrorMessage(event, mediaSourceDetailsList,
									mediaSourceDetails, new FileCopyStatus("Error",
											"File already found"), e);
							
							getAddMediaFilesTableModel(event).setValueAt(
									new Boolean(true),
									mediaSourceDetailsList
											.indexOf(mediaSourceDetails), 9);
							mediaSourceDetails.setOverwriteMedia(new Boolean(
									true));
							mediaSourceDetailsList.set(mediaSourceDetailsList
									.indexOf(mediaSourceDetails),
									mediaSourceDetails);
							ErrorMessage.getMediaFileDuplicationErrorMessage(e.getMessage());
						} catch (ConnectionNotAvaliableException e) {
							ErrorMessage.getDataBaseErrorMessage(e.getMessage());

						} catch (IOException e) {
							ErrorMessage
									.getIOErrorMessage("Please check the source file or permissions of source file.");
						} catch (MediaFileException e) {
							ErrorMessage.getMediaFileNotFoundErrorMessage(e.getMessage());
						}

					}
					fileCount = fileCount + 1;
					if (initiateCopy) {
						int allFiles = fileCount;

						progressBar.setValue(fileCount * 100 / allFiles);
						if (progressBar.getValue() == 100) {
							operationProgress.dispose();
						}
					}
				} catch (IncorrectInput e1) {
					ErrorMessage.getIncorrectInput();
					dispose = false;
				}
			}

			deleteSuccessRows(removeMediaSourceDetailsIndex, event);

		} catch (InstantiationException e) {
			ErrorMessage.getErrorMessage();
		} catch (IllegalAccessException e) {
			ErrorMessage.getErrorMessage();
		} catch (ClassNotFoundException e) {
			ErrorMessage.getErrorMessage();
		} catch (InterruptedException e) {
			ErrorMessage.getErrorMessage();
		} catch (SQLException e) {
			ErrorMessage
					.getDataBaseErrorMessage("Problem with Database connection please try again later.");
		}
		if (dispose) {
			closeAddMediaBox(event);
		}
	}

	private MediaSourceDetails setErrorMessage(ActionEvent event,
			ArrayList<MediaSourceDetails> mediaSourceDetailsList,
			MediaSourceDetails mediaSourceDetails,
			FileCopyStatus fileCopyStatus, Exception e) {
		
		mediaSourceDetails.setCopyStatus(fileCopyStatus);
		getAddMediaFilesTableModel(event).setValueAt(
				(Object) mediaSourceDetails.getCopyStatus().getStatusMessage(),
				mediaSourceDetailsList.indexOf(mediaSourceDetails), 8);
		
		return mediaSourceDetails;
	}

	private void deleteSuccessRows(
			ArrayList<Integer> removeMediaSourceDetailsIndex, ActionEvent event) {
		ListIterator<Integer> listIterator = removeMediaSourceDetailsIndex
				.listIterator(removeMediaSourceDetailsIndex.size());
		while (listIterator.hasPrevious()) {
			getAddMediaFilesTableModel(event).removeRow(
					(Integer) listIterator.previous());

		}
	}

	private void closeAddMediaBox(ActionEvent event) {
		AddNewMediaView addNewMediaView = (AddNewMediaView) ((JButton) event
				.getSource()).getTopLevelAncestor();
		addNewMediaView.dispose();
	}

	public AddMediaFilesTableModel getAddMediaFilesTableModel(ActionEvent event) {
		return (AddMediaFilesTableModel) ((AddMediaFilesTable) ((JScrollPane) ((JButton) event
				.getSource()).getParent().getParent().getComponents()[1])
				.getViewport().getComponents()[0]).getModel();
	}

	public AddMediaFilesTable getAddMediaFilesTable(ActionEvent event) {
		return ((AddMediaFilesTable) ((JScrollPane) ((JButton) event
				.getSource()).getParent().getParent().getComponents()[1])
				.getViewport().getComponents()[0]);
	}
}
