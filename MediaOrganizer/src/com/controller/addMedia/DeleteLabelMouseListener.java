package com.controller.addMedia;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import com.model.viewModel.AddMediaFilesTableModel;
import com.view.addNewMedia.AddMediaFilesTable;

public class DeleteLabelMouseListener implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent event) {
		AddMediaFilesTable table = getAddMediaFilesTable(event);
		int i;
		if (((AddMediaFilesTableModel) table.getModel()).getRowCount() > 0) {

			System.out.println(table.getSelectedRowCount());
			int[] selectedTableIndices = table.getSelectedRows();
			if (selectedTableIndices.length > 0) {

				for (i = 0; i < table.getSelectedRowCount(); i++) {
					System.out.println("Element at "
							+ selectedTableIndices[i]
							+ " is "
							+ ((AddMediaFilesTableModel) table.getModel())
									.getValueAt(i, 0)
							+ "  "
							+ ((AddMediaFilesTableModel) table.getModel())
									.getValueAt(i, 1));
					((DefaultTableModel) table.getModel()).removeRow(i);
				}

			} else {
				((AddMediaFilesTableModel) table.getModel()).removeRow(0);
			}
		}
	}

	private AddMediaFilesTable getAddMediaFilesTable(MouseEvent event) {
		return ((AddMediaFilesTable) ((JScrollPane) ((JLabel) event.getSource()).getParent().getParent().getComponents()[1]).getViewport()
				.getComponents()[0]);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
