package com.Utils.inputValidation;


import com.model.domainModel.MediaSourceDetails;
import com.model.globalVariables.GlobalVariables;

import exceptions.IncorrectInput;

public class MediaInputValidate {

	public static  void getMediaType(MediaSourceDetails mediaSourceDetails) throws IncorrectInput{
		
		String mediaType = mediaSourceDetails.getMediaType();

		Boolean audio = mediaSourceDetails.getAudioFile();
		Boolean video = mediaSourceDetails.getVideoFile();
		Boolean book = mediaSourceDetails.geteBookFile();
		
			if (mediaType.equals(GlobalVariables.getMovieAsMediaType())) {
				if(video != true){
					throw new IncorrectInput();
				}
			}
			
			if (mediaType.equals(GlobalVariables.getMusicAsMediaType())) {
				if(video == true || audio != true){
					throw new IncorrectInput();
				}
			}
			
			if (mediaType.equals(GlobalVariables.getBookAsMediaType())) {
				if(video == true || audio == true || book != true){
					throw new IncorrectInput();
				}
			}
		
	}
}
