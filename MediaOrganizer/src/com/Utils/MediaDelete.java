package com.Utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class MediaDelete {

	public static void deleteFile(String fileName){
		try {
			FileUtils.deleteDirectory(new File(fileName));
		} catch (IOException e) {
			System.err.println("Following file not found: "+fileName);
		}
	}
	
	public static Boolean filePresent(String fileName){
		return new File(fileName).exists();
	}
	
	public static void main(String[] args){
		System.out.println("File Operation");
		MediaDelete.deleteFile("/home/uday_reddy/MediaOrganizer/Movie/iron man");
		System.out.println(MediaDelete.filePresent("/home/uday_reddy/MediaOrganizer/Movie/iron man"));
	}
	
}
