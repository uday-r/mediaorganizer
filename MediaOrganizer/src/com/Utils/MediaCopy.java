package com.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FilenameUtils;

import com.model.domainModel.MediaSourceDetails;

import ebook.EBook;
import ebook.parser.InstantParser;
import ebook.parser.Parser;
import eu.medsea.mimeutil.MimeUtil2;

public class MediaCopy {

	public static void initiateCopy(String sourcePath, String destinationPath,
			MediaSourceDetails mediaSourceDetails){

		File sourceFile = new File(sourcePath);
		File destinationFile = new File(destinationPath + "/"
				+ sourceFile.getName());
		System.out.println("Destination exists: "+destinationFile.exists());
		if (sourceFile.isDirectory()) {
			if (!destinationFile.exists()) {
				destinationFile.mkdir();
			}

			String fileNames[] = sourceFile.list();
			for (String file : fileNames) {
				initiateCopy(sourceFile.getAbsolutePath() + "/" + file,
						destinationFile.getAbsolutePath(), mediaSourceDetails);
			}

		} else {

				if (getFileCopyConfimation(sourceFile, mediaSourceDetails)) {

					InputStream inStream = null;
					OutputStream outStream = null;

					try {
						inStream = new FileInputStream(sourceFile);
						outStream = new FileOutputStream(destinationFile);

						byte[] buffer = new byte[1024];

						int length;
						double increaseSize = 0;
						while ((length = inStream.read(buffer)) > 0) {
							increaseSize = increaseSize + length;
							outStream.write(buffer, 0, length);
						}

						inStream.close();
						outStream.close();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.err.println("File not found");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.err.println("Input Output error!!");
					}
					
				}

			
		}
	}

	private static Boolean getFileCopyConfimation(File SourceFile,
			MediaSourceDetails mediaSourceDetails) {

		Boolean poster = mediaSourceDetails.getPoster();
		Boolean subtitle = mediaSourceDetails.getSubtitle();
		Boolean video = mediaSourceDetails.getVideoFile();
		Boolean audio = mediaSourceDetails.getAudioFile();
		Boolean book = mediaSourceDetails.geteBookFile();

		Boolean sourceFilePoster = false;
		Boolean sourceFileSubtitle = false;
		Boolean sourceFileVideo = false;
		Boolean sourceFileAudio = false;
		Boolean sourceFileBook = false;

		String path = SourceFile.getAbsolutePath();

		MimeUtil2 mimeUtil = new MimeUtil2();
		mimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
		String mediaFileType = MimeUtil2
				.getMostSpecificMimeType(mimeUtil.getMimeTypes(SourceFile))
				.toString().split("/")[0];

		if (mediaFileType.equals("image")) {
			sourceFilePoster = true;
			if (poster && sourceFilePoster) {
				return true;
			}
		}
		String mediaFileExtension = FilenameUtils.getExtension(path);
		if (mediaFileExtension.equals("srt")) {
			sourceFileSubtitle = true;
			if (subtitle && sourceFileSubtitle) {
				return true;
			}
		}
		if (mediaFileType.equals("video")) {
			sourceFileVideo = true;
			if (video && sourceFileVideo) {
				return true;
			}
		}
		if (mediaFileType.equals("audio")) {
			sourceFileAudio = true;
			if (audio && sourceFileAudio) {
				return true;
			}
		}
		Parser parser = new InstantParser();
		EBook ebook = parser.parse(path);
		if (ebook.isOk) {
			sourceFileBook = true;
			if (book && sourceFileBook) {
				return true;
			}
		}
		return false;
	}

}