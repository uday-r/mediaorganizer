package com.Utils.fileOperations;

import java.awt.Image;

import javax.swing.ImageIcon;

import com.view.addNewMedia.AddNewMediaView;



public class ImageOperation {

	public static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = AddNewMediaView.class.getClassLoader().getResource("com/view/"+path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + "/com/view/"+path);
			return null;
		}
	}	

	public static Image scaleImage(ImageIcon mediaIcon, int maxSize) {
		float iconHeight = mediaIcon.getIconHeight();
		float iconWidth = mediaIcon.getIconWidth();
		int newIconHeight = 0;
		int newIconWidth = 0;
		if (iconWidth > iconHeight) {
			newIconHeight = maxSize;
			newIconWidth = (int) (iconHeight / iconWidth) * maxSize;
		} else if (iconHeight > iconWidth) {
			newIconHeight = maxSize;
			newIconWidth = (int) ((iconWidth / iconHeight) * maxSize);
		} else {
			newIconWidth = maxSize;
			newIconHeight = maxSize;
		}
		iconHeight = (int) newIconHeight;
		iconWidth = (int) newIconWidth;
		Image mediaImage = mediaIcon.getImage();
		mediaImage = mediaImage.getScaledInstance(newIconWidth, newIconHeight,
				java.awt.Image.SCALE_SMOOTH);
		return mediaImage;
	}
}
