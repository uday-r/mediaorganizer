package com.Utils.fileOperations;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.Utils.ErrorMessage;

public class MediaDelete {

	public static void deleteFile(String fileName) {
		try {
			FileUtils.deleteDirectory(new File(fileName));
		} catch (IOException e) {
			ErrorMessage.getIOErrorMessage("Please check if file is present or if the permissions to the file are proper");
		}
	}

	public static Boolean filePresent(String fileName) {
		return new File(fileName).exists();
	}

}
