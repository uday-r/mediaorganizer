package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.model.globalVariables.GlobalVariables;

public class MediaOrganiserDatabaseConnection {
	private Connection mediaOrganiserDatabaseConnection = null;

	public MediaOrganiserDatabaseConnection() throws SQLException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		String connectionUrl = "jdbc:mysql://localhost:3306/"+GlobalVariables.getDatabasename();
		String connectionUser = "root";
		String connectionPassword = "8147312585";
		mediaOrganiserDatabaseConnection = DriverManager.getConnection(
				connectionUrl, connectionUser, connectionPassword);
	}
	
	public Connection getMediaOrganiserDatabaseConnection(){
		return this.mediaOrganiserDatabaseConnection;
	}
}
