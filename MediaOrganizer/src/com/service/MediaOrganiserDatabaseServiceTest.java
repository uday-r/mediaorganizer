package com.service;

import java.sql.SQLException;


import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import com.model.domainModel.FileCopyStatus;
import com.model.domainModel.MediaSourceDetails;
import com.service.MediaOrganiserDatabaseService;

public class MediaOrganiserDatabaseServiceTest {

	
	MediaSourceDetails mediaSourceDetails;
	MediaOrganiserDatabaseService mediaOrganiserDatabaseService;
	
	@Before
	public void setup() throws IllegalAccessException, ClassNotFoundException, InstantiationException, SQLException{
		
		mediaOrganiserDatabaseService = MediaOrganiserDatabaseService.getMediaOrganiserInstance();
		int rowCountBefore = mediaOrganiserDatabaseService.getMediaCount("select Movie_Name from Movie;");
		System.out.println(rowCountBefore);
		mediaSourceDetails = new MediaSourceDetails("Movie", "iron man", true, false, true, false, false,"/home/uday_reddy/movies/iron man/");
	}
	
	@Test
	public void testAddNewMovie() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		System.out.println("Test Start");
		FileCopyStatus insertNewMovie = mediaOrganiserDatabaseService.insertNewMovie(mediaSourceDetails);
		System.out.println(insertNewMovie.getStatusMessage());
		try {
			assertEquals("Value inserted is",1,mediaOrganiserDatabaseService.getMediaCount("select Movie_Name from Movie where Movie_Name='"+mediaSourceDetails.getMediaName()+"';"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mediaOrganiserDatabaseService.deleteMedia(mediaSourceDetails);
		int rowCountAfter = mediaOrganiserDatabaseService.getMediaCount("select Movie_Name from Movie;");
		System.out.println(rowCountAfter);
//		//after incrementing delete the inserted record 
		
	}
	public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException, InstantiationException, SQLException{
		new MediaOrganiserDatabaseServiceTest();
	}
}