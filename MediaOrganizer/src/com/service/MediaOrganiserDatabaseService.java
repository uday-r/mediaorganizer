package com.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JProgressBar;


import com.controller.operationProgressInitiator.MediaFileCopyInitiator;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;
import com.model.globalVariables.GlobalVariables;
import com.model.domainModel.FileCopyStatus;
import com.model.domainModel.MediaSourceDetails;
import com.model.domainModel.MediaTypeDetails;
import com.view.operationProgress.OperationProgress;

public class MediaOrganiserDatabaseService {

	private static Connection mediaOrganiserDatabaseConnection = null;
	int temp = 0;
	private static MediaOrganiserDatabaseService mediaOrganiserDatabaseService = null;


	private MediaOrganiserDatabaseService() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		mediaOrganiserDatabaseConnection = createMediaOrganiserDatabaseConnection();
	}

	public static MediaOrganiserDatabaseService getMediaOrganiserInstance()
			throws IllegalAccessException, ClassNotFoundException,
			SQLException, InstantiationException {
		if (mediaOrganiserDatabaseService == null) {
			synchronized (MediaOrganiserDatabaseConnection.class) {
				if (mediaOrganiserDatabaseService == null) {
					mediaOrganiserDatabaseService = new MediaOrganiserDatabaseService();
				}
			}
		}
		return mediaOrganiserDatabaseService;
	}

	private static Connection createMediaOrganiserDatabaseConnection()
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, SQLException {
		MediaOrganiserDatabaseConnection mediaOrganiserDatabaseConnection = new MediaOrganiserDatabaseConnection();
		return (Connection) mediaOrganiserDatabaseConnection
				.getMediaOrganiserDatabaseConnection();
	}

	private String replaceSpaceWithUnderscore(Object name) {
		return ((String) name).replaceAll(" ", "_");
	}

	private String mediaSearchQueryFactory(MediaTypeDetails mediaDetails) {
		Object mediaType = mediaDetails.getMediaType();
		String mediaTypeWithUnderScore = replaceSpaceWithUnderscore(mediaType);
		Object columnName = mediaDetails.getMediaTypeProperty();
		String columnTypename = null;
		if (columnName != null)
			columnTypename = replaceSpaceWithUnderscore(columnName);
		String listMoviesSearchedQuery = null;
		String media = mediaDetails.getMedia();
		if (media == null) {
			listMoviesSearchedQuery = "select Movie_Name ,Year ,Movie_Rating ,ImdbVotes ,Released ,Runtime ,Genres ,Directors ,Writer ,Actors ,Plot ,Poster ,ImdbID ,Type ,FilePath ,MediaOrganiserFilePath from "
					+ mediaTypeWithUnderScore;
		} else {

			if (columnName == "Movie Rating") {
				if (mediaTypeWithUnderScore.isEmpty()) {
					listMoviesSearchedQuery = "select * from "
							+ mediaTypeWithUnderScore + " where "
							+ columnTypename + " <= 0;";
				} else {
					listMoviesSearchedQuery = "select * from "
							+ mediaTypeWithUnderScore + " where "
							+ columnTypename + " <= " + media + ";";
				}

			} else {
				listMoviesSearchedQuery = "select Movie_Name ,Year ,Movie_Rating ,ImdbVotes ,Released ,Runtime ,Genres ,Directors ,Writer ,Actors ,Plot ,Poster ,ImdbID ,Type ,FilePath ,MediaOrganiserFilePath from "
						+ mediaTypeWithUnderScore
						+ " where "
						+ columnTypename
						+ " like '%" + media + "%';";
			}
		}
		return listMoviesSearchedQuery;
	}

	private ResultSet executeSelectQuery(String query) throws SQLException {
		Statement statement = mediaOrganiserDatabaseConnection
				.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		return resultSet;

	}

	public ArrayList<ArrayList<String>> getSearchedMedia(
			MediaTypeDetails mediaDetails) throws SQLException {

		String listMoviesSearchedQuery = mediaSearchQueryFactory(mediaDetails);

		ArrayList<ArrayList<String>> allMoviesDetailsList = new ArrayList<ArrayList<String>>();

		ResultSet listMoviesSearchedResultSet = executeSelectQuery(listMoviesSearchedQuery);

		while (listMoviesSearchedResultSet.next()) {
			ArrayList<String> movieDetails = new ArrayList<String>();
			ResultSetMetaData moviesMetaData = (ResultSetMetaData) listMoviesSearchedResultSet
					.getMetaData();
			int moviesColumns = moviesMetaData.getColumnCount();
			String moviesDetailColumn;
			for (int i = 1; i <= moviesColumns; i++) {
				moviesDetailColumn = listMoviesSearchedResultSet.getString(i);
				movieDetails.add(moviesDetailColumn);
			}
			allMoviesDetailsList.add(movieDetails);
		}
		listMoviesSearchedResultSet.close();
		return allMoviesDetailsList;
	}

	private String searchIfMediaAvaliableQueryFactory(String mediaName,
			String mediaType) {
		return "select " + mediaType + "_Name from " + mediaType + " where "
				+ mediaType + "_Name like '" + mediaName + "';";
	}
	
	public int getMediaCount(String query) throws SQLException{
		ResultSet searchIfMediaAvaliableResultSet = executeSelectQuery(query);
		searchIfMediaAvaliableResultSet.last();

		return searchIfMediaAvaliableResultSet.getRow();
	}
	

	private boolean searchIfMediaAvaliable(String mediaName, String mediaType)
			throws SQLException {
		String searchIfMediaAvaliableQuery = searchIfMediaAvaliableQueryFactory(
				mediaName, mediaType);

		ResultSet searchIfMediaAvaliableResultSet = executeSelectQuery(searchIfMediaAvaliableQuery);
		searchIfMediaAvaliableResultSet.last();

		int rowCount = searchIfMediaAvaliableResultSet.getRow();
		searchIfMediaAvaliableResultSet.close();
		if (rowCount > 0) {
			return false;
		} else {
			return true;
		}
	}

	public ArrayList<MediaSourceDetails> insertNewMedia(
			ArrayList<MediaSourceDetails> mediaSourceDetailsList)
			throws SQLException, InterruptedException {

		FileCopyStatus fileCopyStatus = null;
		OperationProgress operationProgress = null;
		JProgressBar progressBar = null;
		Boolean initiateCopy = false;
		if (!mediaSourceDetailsList.get(0).getCopyStatus()
				.equals("Failure:File Already found")) {
			operationProgress = new OperationProgress();
			progressBar = operationProgress.getProgressBar();
			progressBar.setValue(0);
			initiateCopy = true;
		}

		int fileCount = 0;
		for (MediaSourceDetails mediaSourceDetails : mediaSourceDetailsList) {
			if (mediaSourceDetails.getMediaType().equals("Movie")) {
				fileCopyStatus = insertNewMovie(mediaSourceDetails);
				mediaSourceDetails.setCopyStatus(fileCopyStatus);
			}
			else if (mediaSourceDetails.getMediaType().equals("none")) {
				mediaSourceDetails.setCopyStatus(new FileCopyStatus("Failure", "Please add relevant Media Files"));
			}
			fileCount = fileCount + 1;
			if (initiateCopy) {
				progressBar.setValue(fileCount * 100
						/ mediaSourceDetailsList.size());
				if (progressBar.getValue() == 100) {
					operationProgress.dispose();
				}
			}
		}

		return mediaSourceDetailsList;
	}

	public FileCopyStatus insertNewMovie(MediaSourceDetails mediaSourceDetails)
			throws SQLException {

		String movieName = mediaSourceDetails.getMediaName();
		String mediaType = mediaSourceDetails.getMediaType();
		FileCopyStatus fileCopyStatus = null;
		Connection connection = MediaOrganiserDatabaseService.mediaOrganiserDatabaseConnection;
		connection.setAutoCommit(false);
		if (searchIfMediaAvaliable(movieName, mediaType)) {

			String movieSourcePath = mediaSourceDetails.getMediaSourcePath();
			String movieDestinationPath = GlobalVariables
					.getMediaOrganiserPath() + "Movies/" + movieName;

			String query = "INSERT INTO  Movie (Movie_Name ,Year ,Movie_Rating ,ImdbVotes ,Released ,Runtime ,Genres ,Directors ,Writer ,Actors ,Plot ,Poster ,ImdbID ,Type ,FilePath ,MediaOrganiserFilePath) VALUES (?,  '',  0,  '',  '',  '',  '',  '',  '',  '',  '',  '',  '',  '', ?, ?)";
			PreparedStatement insertMovie = null;
			try {

				insertMovie = (PreparedStatement) connection
						.prepareStatement(query);

				insertMovie.setString(1, movieName);
				insertMovie.setString(2, movieSourcePath);
				insertMovie.setString(3, movieDestinationPath);
				insertMovie.executeUpdate();
				connection.commit();

				MediaFileCopyInitiator mediaFileOperationInitiator = new MediaFileCopyInitiator(mediaSourceDetails);
				fileCopyStatus = mediaFileOperationInitiator.iterateFilesForProgressUpdate(
						movieSourcePath,
						GlobalVariables.getMediaOrganiserPath() + mediaType
								+ "/");


			} catch (SQLException e) {
				e.printStackTrace();
				connection.rollback();

			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (insertMovie != null) {
					insertMovie.close();
				}
			}
		} else {
			fileCopyStatus = new FileCopyStatus("Failure", "File Already found");
		}
		connection.setAutoCommit(true);
		return fileCopyStatus;
	}
	
	public void deleteMedia(MediaSourceDetails mediaSourceDetails)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String query = "delete from " + mediaSourceDetails.getMediaType()
				+ " where Movie_Name = '?'";
		PreparedStatement deleteMedia = null;
		Connection connection = createMediaOrganiserDatabaseConnection();

		deleteMedia = (PreparedStatement) connection.prepareStatement("delete from Movie where Movie_Name ='iron man';");

//		deleteMedia.setString(1, mediaSourceDetails.getMediaName());
		deleteMedia.executeUpdate();
		connection.close();
	}

}
