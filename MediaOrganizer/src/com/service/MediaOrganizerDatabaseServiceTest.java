package com.service;

import java.sql.SQLException;


import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import com.model.domainModel.FileCopyStatus;
import com.model.domainModel.MediaSourceDetails;
import com.service.MediaOrganizerDatabaseService;

public class MediaOrganizerDatabaseServiceTest {

	
	MediaSourceDetails mediaSourceDetails;
	MediaOrganizerDatabaseService mediaOrganizerDatabaseService;
	
	@Before
	public void setup() throws IllegalAccessException, ClassNotFoundException, InstantiationException, SQLException{
		
		mediaOrganizerDatabaseService = MediaOrganizerDatabaseService.getMediaOrganizerInstance();
		int rowCountBefore = mediaOrganizerDatabaseService.getMediaCount("select Movie_Name from Movie;");
		System.out.println(rowCountBefore);
		mediaSourceDetails = new MediaSourceDetails("Movie", "iron man", true, false, true, false, false,"/home/uday_reddy/movies/iron man/");
	}
	
	@Test
	public void testAddNewMovie() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		System.out.println("Test Start");
		FileCopyStatus insertNewMovie = mediaOrganizerDatabaseService.insertNewMovie(mediaSourceDetails);
		System.out.println(insertNewMovie.getStatusMessage());
		try {
			assertEquals("Value inserted is",1,mediaOrganizerDatabaseService.getMediaCount("select Movie_Name from Movie where Movie_Name='"+mediaSourceDetails.getMediaName()+"';"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mediaOrganizerDatabaseService.deleteMedia(mediaSourceDetails);
		int rowCountAfter = mediaOrganizerDatabaseService.getMediaCount("select Movie_Name from Movie;");
		System.out.println(rowCountAfter);
//		//after incrementing delete the inserted record 
		
	}
	public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException, InstantiationException, SQLException{
		new MediaOrganizerDatabaseServiceTest();
	}
}