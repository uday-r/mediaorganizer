package com.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Vector;

public class MediaOrganizerDatabaseConnectionPool {

	private static Vector<MediaOrganizerDatabaseConnection> databaseConnections = new Vector<MediaOrganizerDatabaseConnection>();
	static{
		for(int i=0; i<10 ;i++){
				try {
					databaseConnections.add(new MediaOrganizerDatabaseConnection());
				} catch (InstantiationException e){
					
				} catch (IllegalAccessException e){
					
				} catch (ClassNotFoundException e){
					
				} catch (SQLException e) {

				}
		}
	}
	
	public MediaOrganizerDatabaseConnection getAvaliableConnection(){
		MediaOrganizerDatabaseConnection databaseConnection = null;
		if(databaseConnections.size() > 0){
			databaseConnection = databaseConnections.get(databaseConnections.size()-1);
			databaseConnections.remove(databaseConnections.size()-1);
		}
		return databaseConnection;
	}
	
	public void returnConnection(Connection databaseConnection){
		databaseConnections.add(new MediaOrganizerDatabaseConnection(databaseConnection));
	}
}
