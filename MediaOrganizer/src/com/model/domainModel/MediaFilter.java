package com.model.domainModel;

import java.util.ArrayList;

import org.apache.log4j.Logger;

public class MediaFilter {

	private MediaTypeDetails mediaFilterCriteria;
	private ArrayList<ArrayList<String>> searchResults;
	static Logger log = Logger.getLogger(MediaFilter.class.getName());
	
	public MediaFilter(MediaTypeDetails mediaFilterCriteria,
			ArrayList<ArrayList<String>> searchResults) {
		log.info(MediaFilter.class.getName()+" constructor invoked");
		this.mediaFilterCriteria = mediaFilterCriteria;
		log.debug("this.mediaFilterCriteria value is "+ this.mediaFilterCriteria);
		this.searchResults = searchResults;
		log.debug("this.searchResults value is "+ this.searchResults);

	}

	public MediaTypeDetails getMediaFilterCriteria() {
		return mediaFilterCriteria;
	}

	public ArrayList<ArrayList<String>> getSearchResults() {
		return searchResults;
	}

}
