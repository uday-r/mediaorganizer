package com.model.domainModel;

import com.view.mediaOrganizer.ResultSetTable;

public class TableRowData {

	private String name;
	private int year;
	private float rating;
	private String ratingCount;
	private String releaseData;
	private String movielength;
	private String genre;
	private String directors;
	private String writers;
	private String actors;
	private String plot;
	private String poster;
	private String imdbId;
	private String type;
	private String mediaOrganizerFilePath;

	public TableRowData(ResultSetTable table, int row) {
		if (row > -1) {
			this.setName((String) table.getValueAt(row, 0));
			System.out.println("year:" + table.getValueAt(row, 1) + ":");
			if (table.getValueAt(row, 1).equals("")) {
				this.setYear(0);
			} else {

				this.setYear((int) Integer.parseInt((String) table.getValueAt(
						row, 1)));
			}

			this.setRating((float) Float.parseFloat((String) table.getValueAt(
					row, 2)));
			this.setRatingCount((String) table.getValueAt(row, 3));
			this.setReleaseData((String) table.getValueAt(row, 4));
			this.setMovielength((String) table.getValueAt(row, 5));
			this.setGenre((String) table.getValueAt(row, 6));
			this.setDirectors((String) table.getValueAt(row, 7));
			this.setWriters((String) table.getValueAt(row, 8));
			this.setActors((String) table.getValueAt(row, 9));
			this.setPlot((String) table.getValueAt(row, 10));
			this.setPoster((String) table.getValueAt(row, 11));
			this.setImdbId((String) table.getValueAt(row, 12));
			this.setType((String) table.getValueAt(row, 13));
			this.setMediaOrganizerFilePath((String) table.getValueAt(row, 15));
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getRatingCount() {
		return ratingCount;
	}

	public void setRatingCount(String ratingCount) {
		this.ratingCount = ratingCount;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public String getReleaseData() {
		return releaseData;
	}

	public void setReleaseData(String releaseData) {
		this.releaseData = releaseData;
	}

	public String getMovielength() {
		return movielength;
	}

	public void setMovielength(String movielength) {
		this.movielength = movielength;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getDirectors() {
		return directors;
	}

	public void setDirectors(String directors) {
		this.directors = directors;
	}

	public String getWriters() {
		return writers;
	}

	public void setWriters(String writers) {
		this.writers = writers;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getMediaOrganizerFilePath() {
		return mediaOrganizerFilePath;
	}

	public void setMediaOrganizerFilePath(String mediaOrganizerFilePath) {
		this.mediaOrganizerFilePath = mediaOrganizerFilePath;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
