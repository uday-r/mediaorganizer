package com.model.domainModel;

public class MediaTypeDetails {

	private String mediaType;
	private String mediaTypeProperty;
	private String media;
	public MediaTypeDetails(String mediaType, String mediaTypeProperty, String media){
		this.setMediaType(mediaType);
		this.setMediaTypeProperty(mediaTypeProperty);
		this.setMedia(media);
	}
	public String getMediaType() {
		return mediaType;
	}
	private void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	public String getMediaTypeProperty() {
		return mediaTypeProperty;
	}
	private void setMediaTypeProperty(String mediaTypeProperty) {
		this.mediaTypeProperty = mediaTypeProperty;
	}
	public String getMedia() {
		return media;
	}
	private void setMedia(String media) {
		this.media = media;
	}
	
	
}
