package com.model.domainModel;

import java.util.ArrayList;


public class MediaSourceDetails {

	private String mediaType;
	private String mediaName;
	private Boolean poster;
	private Boolean subtitle;
	private Boolean videoFile;
	private Boolean audioFile;
	private Boolean eBookFile;
	private String mediaSourcePath;
	private FileCopyStatus copyStatus;
	private Boolean overwriteMedia;
	public MediaSourceDetails(ArrayList<String> row) {
		this.setMediaType(row.get(0));
		this.setMediaName(row.get(1));
		this.setPoster(new Boolean(row.get(2)).booleanValue());
		this.setSubtitle(new Boolean(row.get(3)).booleanValue());
		this.setVideoFile(new Boolean(row.get(4)).booleanValue());
		this.setAudioFile(new Boolean(row.get(5)).booleanValue());
		this.seteBookFile(new Boolean(row.get(6)).booleanValue());
		this.setMediaSourcePath(row.get(7));
		this.setCopyStatus(new FileCopyStatus("Not Defined",""));
		this.setOverwriteMedia(new Boolean(row.get(8)).booleanValue());
	}

	public MediaSourceDetails(String mediaType , String mediaName, Boolean poster, Boolean subtitle, Boolean video, Boolean audio, Boolean book, String path, Boolean overwriteStatus){
		this.setMediaType(mediaType);
		this.setMediaName(mediaName);
		this.setPoster(new Boolean(poster));
		this.setSubtitle(new Boolean(subtitle));
		this.setVideoFile(new Boolean(video));
		this.setAudioFile(new Boolean(audio));
		this.seteBookFile(new Boolean(book));
		this.setMediaSourcePath(path);
		this.setCopyStatus(new FileCopyStatus("Not Defined",""));
		this.setOverwriteMedia(new Boolean(overwriteStatus));
	}
	
	public String getMediaName() {
		return mediaName;
	}

	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}

	public String getMediaSourcePath() {
		return mediaSourcePath;
	}

	public void setMediaSourcePath(String mediaSourcePath) {
		this.mediaSourcePath = mediaSourcePath;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public Boolean getPoster() {
		return poster;
	}

	public void setPoster(Boolean poster) {
		this.poster = poster;
	}

	public Boolean getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(Boolean subtitle) {
		this.subtitle = subtitle;
	}

	public Boolean getVideoFile() {
		return videoFile;
	}

	public void setVideoFile(Boolean videoFile) {
		this.videoFile = videoFile;
	}

	public Boolean getAudioFile() {
		return audioFile;
	}

	public void setAudioFile(Boolean audioFile) {
		this.audioFile = audioFile;
	}

	public Boolean geteBookFile() {
		return eBookFile;
	}

	public void seteBookFile(Boolean eBookFile) {
		this.eBookFile = eBookFile;
	}

	public FileCopyStatus getCopyStatus() {
		return copyStatus;
	}

	public void setCopyStatus(FileCopyStatus copyStatus) {
		this.copyStatus = copyStatus;
	}

	public Boolean getOverwriteMedia() {
		return overwriteMedia;
	}

	public void setOverwriteMedia(Boolean overwriteMedia) {
		this.overwriteMedia = overwriteMedia;
	}
}
