package com.model.viewModel;

import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultComboBoxModel;

public class SearchByComboBoxModel extends DefaultComboBoxModel implements Observer {



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SearchByComboBoxModel() {
		super();
		addAll_MediaDetailsToComboBox();
	}

	public void addAll_MediaDetailsToComboBox() {
		removeAllElements();
		addElement("Search By");
		addElement("Media Name");
		addElement("Media Rating");
		addElement("Release Data");
		addElement("Category");
	}

	public void addMovieDetailsToComboBox() {
		removeAllElements();
		addElement("Movie Name");
		addElement("Movie Rating");
		addElement("Genres");
		addElement("Writers");
		addElement("Directors");
		addElement("Actors");
		addElement("Year");
		addElement("Country");
	}

	public void addBookDetailsToComboBox() {
		removeAllElements();
		addElement("Book Name");
		addElement("Rating");
		addElement("Genres");
		addElement("Authors");
		addElement("Year");
		addElement("Publisher");
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("\n Got it!!!");
		
	}
}
