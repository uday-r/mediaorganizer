package com.model.viewModel;


import javax.swing.table.DefaultTableModel;


public class AddMediaFilesTableModel extends DefaultTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String mediaType;

	public AddMediaFilesTableModel(String mediaType) {
		super();
		this.mediaType = mediaType;
		addColumnsToModel();
	}

	
	private void addColumnsToModel() {
		addColumn("Type");
		addColumn("Name");
		addColumn("Poster");
		addColumn("Subtitle");
		addColumn("Video File");
		addColumn("Audio File");
		addColumn("E-Book File");
		addColumn("Path");
		addColumn("Error Type");
		addColumn("Over write");
	}

	@Override
	public boolean isCellEditable(int row, int column) {

		if (column == 0 || column >= 9)
			return false;
		else
			return true;
	}
	
}
