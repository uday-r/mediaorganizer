package com.model.globalVariables;

public class GlobalVariables {

	public static String getMediaOrganizerPath(){
		return "/home/uday_reddy/MediaOrganizer/";
	}
	public static String getDatabasename(){
		return "MediaOrganizer";
	}
	
	public static String getMovieFolder(){
		return "Movies/";
	}
	
	public static String getMovieAsMediaType(){
		return "Movie";
	}
	
	public static String getMusicAsMediaType(){
		return "Music";
	}

	public static String getBookAsMediaType(){
		return "Book";
	}

}
