package com.view.mediaOrganizer;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import org.apache.log4j.Logger;


public class MediaTypeComboBox extends JComboBox{
	/**
	 * 
	 */
	static Logger log = Logger.getLogger(RowSelectionButton.class.getName());
	private static final long serialVersionUID = 1L;
	private static Object selectedItem;
	private static DefaultComboBoxModel mediaTypeComboBoxModel = new DefaultComboBoxModel(
			new Object[] { "All_Media", "Movie", "Book" });
	public MediaTypeComboBox(){
		super(mediaTypeComboBoxModel);
		log.info(RowSelectionButton.class.getName()+" constructor invoked");
		setSelectedItem("All Media");
		log.debug("set Selected Item to 'All Media'");
	}
	

}
