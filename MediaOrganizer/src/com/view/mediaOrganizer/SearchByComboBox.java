package com.view.mediaOrganizer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComboBox;

import org.apache.log4j.Logger;

import com.Utils.ErrorMessage;
import com.model.domainModel.MediaFilter;
import com.model.domainModel.MediaTypeDetails;
import com.model.viewModel.SearchByComboBoxModel;

@SuppressWarnings("rawtypes")
public class SearchByComboBox extends JComboBox implements Observer {

	/**
	 * 
	 */
	static Logger log = Logger.getLogger(SearchByComboBox.class.getName());
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public SearchByComboBox() {
		super(new SearchByComboBoxModel());
		log.info(SearchByComboBox.class.getName()+" constructor invoked");

	}

	

	@Override
	public void update(Observable o, Object mediaFilter) {
		log.info("update block invoked");
		MediaTypeDetails mediaFilterCriteria = ((MediaFilter) mediaFilter).getMediaFilterCriteria();
		invokeComboBoxContentChangeMethod((String) mediaFilterCriteria.getMediaType());
		
	}

	private String getModelClassName() {
		log.debug("Model Class Name is "+getModel().getClass().getName());
		return getModel().getClass().getName();
	}

	@SuppressWarnings("unchecked")
	private void invokeComboBoxContentChangeMethod(String mediaDetails) {
		log.info("invokeComboBoxContentChangeMethod");
		
		Class noParams[] = {};
		log.debug("noParams value is "+noParams);
		
		Method addDetailsToColumn = null;
		log.debug("addDetailsToColumn value is "+addDetailsToColumn);
		try {

			String className = getModelClassName();
			log.debug("className value is "+ className);
			Class searchByComboBoxModelClass = Class.forName(className);
			log.debug("searchByComboBoxModelClass value is "+ searchByComboBoxModelClass);
			addDetailsToColumn = searchByComboBoxModelClass.getDeclaredMethod(
					"add" + mediaDetails + "DetailsToComboBox",
					noParams);
			log.debug("addDetailsToColumn value is "+addDetailsToColumn);
			addDetailsToColumn.invoke(((SearchByComboBoxModel) getModel()),
					null);
			log.debug("set addDetailsToColumn.invoke accepts 2 variables "+((SearchByComboBoxModel) getModel())+"and null");
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage()+ " reason " +e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (IllegalAccessException e) {
			log.error(e.getMessage()+ " reason " +e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (NoSuchMethodException e) {
			log.error(e.getMessage()+ " reason " +e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (SecurityException e) {
			log.error(e.getMessage()+ " reason " +e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (InvocationTargetException e) {
			log.error(e.getMessage()+ " reason " +e.getCause());
			ErrorMessage.getErrorMessage();
		}
	}
}