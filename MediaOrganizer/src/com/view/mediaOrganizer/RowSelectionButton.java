package com.view.mediaOrganizer;


import org.apache.log4j.Logger;

import com.view.mediaOrganizer.Button;
import com.model.domainModel.TableRowSelectionResponse;

public class RowSelectionButton extends Button {

	/**
	 * 
	 */
	static Logger log = Logger.getLogger(RowSelectionButton.class.getName());
	private TableRowSelectionResponse tableRowSelectionResponse;
	private static final long serialVersionUID = 1L;

	public RowSelectionButton(String buttonName){
		super(buttonName);
		log.info(RowSelectionButton.class.getName()+" constructor invoked");
		
		setEnabled(false);
		log.debug("button disabled");
		
	}

	public void setDisabled(){
		setEnabled(false);
		log.debug("button disabled");
	}
	
	public TableRowSelectionResponse getTableRowSelectionResponse(){
		log.debug("return tableRowSelectionResponse "+tableRowSelectionResponse.getButtonEnabled());
		return tableRowSelectionResponse;
	}
}
