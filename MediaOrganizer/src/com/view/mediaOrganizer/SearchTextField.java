package com.view.mediaOrganizer;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.model.domainModel.MediaTypeDetails;
public class SearchTextField extends JTextField implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Logger log = Logger.getLogger(SearchTextField.class.getName());

	public SearchTextField(int size){
		super(size);
		log.info(SearchTextField.class.getName()+" constructor invoked");
	}

	
	@Override
	public void update(Observable o, Object arg) {
		log.info("update block invoked");
		MediaTypeDetails mediaTypeDetails = (MediaTypeDetails)arg;
		setText("Start Typing the "+mediaTypeDetails.getMediaTypeProperty());
		log.debug("set the SearchTextField value to "+"Start Typing the "+mediaTypeDetails.getMediaTypeProperty());
		
	}
}
