package com.view.mediaOrganizer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTable;

import org.apache.log4j.Logger;

import com.Utils.ErrorMessage;
import com.model.domainModel.MediaFilter;
import com.model.domainModel.MediaTypeDetails;
import com.model.viewModel.ResultSetTableModel;

public class ResultSetTable extends JTable implements Observer {

	/**
	 * 
	 */
	static Logger log = Logger.getLogger(ResultSetTable.class.getName());
	private static final long serialVersionUID = 1L;
	static ResultSetTableModel tableModel = new ResultSetTableModel();

	public ResultSetTable() {
		super(tableModel);
		log.info(ResultSetTable.class.getName()+" constructor invoked");
	}

	
	@Override
	public void update(Observable observable, Object mediaFilter) {
		log.info("update block invoked");
		String observableCLassName = observable.getClass().getSimpleName()
				.toString();
		log.debug("observableCLassName value is "+observableCLassName);
		MediaTypeDetails mediaFilterCriteria = ((MediaFilter) mediaFilter)
				.getMediaFilterCriteria();
		log.debug("mediaFilterCriteria value is " + mediaFilterCriteria);
		
		if (observableCLassName.equals("MediaTypeComboActionListener")) {
			log.debug("observableCLassName value is " + observableCLassName);
			changeColumnsOfTable((String) mediaFilterCriteria.getMediaType());
			
			ArrayList<ArrayList<String>> searchedResults = ((MediaFilter) mediaFilter)
					.getSearchResults();
			
			reloadTableModel(searchedResults);
		} else {
			System.out.println("Filter entered");
			ArrayList<ArrayList<String>> searchedResults = ((MediaFilter) mediaFilter)
					.getSearchResults();
			reloadTableModel(searchedResults);
		}
	}

	private void reloadTableModel(ArrayList<ArrayList<String>> searchedResults) {
		log.info("entered reloadTableModel");
		tableModel.clearModel();
		addSearchedResultsToTableModel(searchedResults);
	}

	private void addSearchedResultsToTableModel(ArrayList<ArrayList<String>> searchedResults){
		log.info("addSearchedResultsToTableModel block encountered");
				for(ArrayList<String> row : searchedResults){
		
				tableModel.addRow(row.toArray());
				log.debug("add "+row.toArray()+" tableModel");
	
				}
	}

	private String getModelClassName() {
		return getModel().getClass().getName();
	}

	@SuppressWarnings("unchecked")
	private void changeColumnsOfTable(String mediaType) {
		log.debug("mediaType value is " + mediaType);
		try {
			@SuppressWarnings("rawtypes")
			
			Class noParams[] = {};
			log.debug("noParams value is "+noParams);
			
			String className = getModelClassName();
			log.debug("className value is "+className);
			
			Method addDetailsToColumn = null;
			log.debug("addDetailsToColumn value is "+addDetailsToColumn);
			
			@SuppressWarnings("rawtypes")
			Class resultSetTableModelClass = Class.forName(className);
			log.debug("resultSetTableModelClass value is "+resultSetTableModelClass);
			
			addDetailsToColumn = resultSetTableModelClass.getDeclaredMethod(
					"add" + mediaType + "DetailsToColumn", noParams);
			log.debug("addDetailsToColumn value is "+addDetailsToColumn);
			
			addDetailsToColumn.invoke(((ResultSetTableModel) getModel()),null);
			log.debug("addDetailsToColumn value is "+addDetailsToColumn);
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage()+" reason "+e.getCause()+"Stack trace");
			ErrorMessage.getErrorMessage();
		} catch (IllegalAccessException e) {
			log.error(e.getMessage()+" reason "+e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (NoSuchMethodException e) {
			log.error(e.getMessage()+" reason "+e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (SecurityException e) {
			log.error(e.getMessage()+" reason "+e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage()+" reason "+e.getCause());
			ErrorMessage.getErrorMessage();
		} catch (InvocationTargetException e) {
			log.error(e.getMessage()+" reason "+e.getCause());
			ErrorMessage.getErrorMessage();
		}
	}

}
