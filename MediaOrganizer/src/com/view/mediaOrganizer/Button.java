package com.view.mediaOrganizer;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.UIManager;

import org.apache.log4j.Logger;

import com.controller.mediaOrganizer.MediaOrganizerController;

public class Button extends JButton{

	/**
	 * 
	 */
	static Logger log = Logger.getLogger(Button.class.getName());
	private static final long serialVersionUID = 1L;

	public Button(String buttonLabel){
		super(buttonLabel);
		log.info(Button.class.getName()+" constructor invoked");
		UIManager.put("Button.foreground", Color.WHITE);
		UIManager.put("Button.background", new Color(0x36c0ec));
	}
}
