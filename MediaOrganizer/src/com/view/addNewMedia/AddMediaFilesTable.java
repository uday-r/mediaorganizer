package com.view.addNewMedia;


import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;

import com.model.viewModel.AddMediaFilesTableModel;

public class AddMediaFilesTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AddMediaFilesTable(AddMediaFilesTableModel tableModel) {
		super(tableModel);
		hidePathColumn();
		addCheckBoxToFirstColumn(); 
	}

	private void addCheckBoxToFirstColumn(){
		JComboBox comboBox = new JComboBox();
        comboBox.addItem("Movie");
        comboBox.addItem("Music");
        comboBox.addItem("Book");
        comboBox.addItem("None");
		getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(comboBox));
	}
	@Override
    public Class getColumnClass(int column) {
        switch (column) {
            
            case 1:
                return String.class;
            case 2:
                return Boolean.class;
            case 3:
                return Boolean.class;
            case 4:
                return Boolean.class;
            case 5:
                return Boolean.class;
            case 6:
                return Boolean.class;
            case 9:
            	return Boolean.class;
            default:
            	return String.class;
        }
    }
	private void hidePathColumn() {
		getColumnModel().getColumn(7).setMaxWidth(0);
		getColumnModel().getColumn(7).setMinWidth(0);
		getColumnModel().getColumn(7).setWidth(0);
		getColumnModel().getColumn(7).setPreferredWidth(0);
		
		getColumnModel().getColumn(8).setMaxWidth(0);
		getColumnModel().getColumn(8).setMinWidth(0);
		getColumnModel().getColumn(8).setWidth(0);
		getColumnModel().getColumn(8).setPreferredWidth(0);
		
//		getColumnModel().getColumn(9).setMaxWidth(0);
//		getColumnModel().getColumn(9).setMinWidth(0);
//		getColumnModel().getColumn(9).setWidth(0);
//		getColumnModel().getColumn(9).setPreferredWidth(0);
	}

	public void hideFolderDetailColumns() {
		getColumnModel().getColumn(2).setMaxWidth(0);
		getColumnModel().getColumn(2).setMinWidth(0);
		getColumnModel().getColumn(2).setWidth(0);
		getColumnModel().getColumn(2).setPreferredWidth(0);

		getColumnModel().getColumn(3).setMaxWidth(0);
		getColumnModel().getColumn(3).setMinWidth(0);
		getColumnModel().getColumn(3).setWidth(0);
		getColumnModel().getColumn(3).setPreferredWidth(0);

		getColumnModel().getColumn(4).setMaxWidth(0);
		getColumnModel().getColumn(4).setMinWidth(0);
		getColumnModel().getColumn(4).setWidth(0);
		getColumnModel().getColumn(4).setPreferredWidth(0);

		getColumnModel().getColumn(5).setMaxWidth(0);
		getColumnModel().getColumn(5).setMinWidth(0);
		getColumnModel().getColumn(5).setWidth(0);
		getColumnModel().getColumn(5).setPreferredWidth(0);

		getColumnModel().getColumn(6).setMaxWidth(0);
		getColumnModel().getColumn(6).setMinWidth(0);
		getColumnModel().getColumn(6).setWidth(0);
		getColumnModel().getColumn(6).setPreferredWidth(0);

		getColumnModel().getColumn(8).setMaxWidth(300);
		getColumnModel().getColumn(8).setMinWidth(300);
		getColumnModel().getColumn(8).setWidth(300);
		getColumnModel().getColumn(8).setPreferredWidth(300);
		
		getColumnModel().getColumn(9).setMaxWidth(100);
		getColumnModel().getColumn(9).setMinWidth(100);
		getColumnModel().getColumn(9).setWidth(100);
		getColumnModel().getColumn(9).setPreferredWidth(100);
	}
}
