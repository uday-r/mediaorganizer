package com.view.mediaOrganiser;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class RowSelectionEditButton extends RowSelectionButton implements
		Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RowSelectionEditButton(String buttonName) {
		super(buttonName);
	}

	@Override
	public void update(Observable arg0, Object buttonEnableStatus) {

		@SuppressWarnings("unchecked")
		ArrayList<Boolean> buttonEnabledStatus = (ArrayList<Boolean>) buttonEnableStatus;
		setEnabled(buttonEnabledStatus.get(0));

	}

}
