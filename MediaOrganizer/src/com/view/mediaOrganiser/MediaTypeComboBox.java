package com.view.mediaOrganiser;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import com.controller.mediaOrganiser.MediaTypeComboActionListener;

public class MediaTypeComboBox extends JComboBox{
	private static Object selectedItem;
	private static DefaultComboBoxModel mediaTypeComboBoxModel = new DefaultComboBoxModel(
			new Object[] { "All_Media", "Movie", "Book" });
	public MediaTypeComboBox(){
		super(mediaTypeComboBoxModel);
		selectedItem = "All Media";
	}
	

}
