package com.view.mediaOrganiser;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextField;

import com.model.domainModel.MediaTypeDetails;
public class SearchTextField extends JTextField implements Observer{

	public SearchTextField(int size){
		super(size);
	}

	
	@Override
	public void update(Observable o, Object arg) {
		MediaTypeDetails mediaTypeDetails = (MediaTypeDetails)arg;
		setText("Start Typing the "+mediaTypeDetails.getMediaTypeProperty());
		
	}
}
