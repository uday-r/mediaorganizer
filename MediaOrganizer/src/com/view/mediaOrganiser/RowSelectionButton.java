package com.view.mediaOrganiser;


import com.view.mediaOrganiser.Button;
import com.model.domainModel.TableRowSelectionResponse;

public class RowSelectionButton extends Button {

	/**
	 * 
	 */
	private TableRowSelectionResponse tableRowSelectionResponse;
	private static final long serialVersionUID = 1L;

	public RowSelectionButton(String buttonName){
		super(buttonName);
		setEnabled(false);
		
	}

	public void setDisabled(){
		setEnabled(false);

	}
	
	public TableRowSelectionResponse getTableRowSelectionResponse(){
		return tableRowSelectionResponse;
	}
}
