package com.view.mediaOrganiser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.model.domainModel.MediaFilter;
import com.model.domainModel.MediaTypeDetails;
import com.model.viewModel.ResultSetTableModel;
import com.controller.mediaOrganiser.SearchByTextBoxKeyListener;

public class ResultSetTable extends JTable implements Observer {

	static ResultSetTableModel tableModel = new ResultSetTableModel();

	public ResultSetTable() {
		super(tableModel);
	}

	@Override
	public void update(Observable observable, Object mediaFilter) {
		
		String observableCLassName = observable.getClass().getSimpleName().toString();
		MediaTypeDetails mediaFilterCriteria = ((MediaFilter) mediaFilter).getMediaFilterCriteria();
		if(observableCLassName.equals("MediaTypeComboActionListener")){
			changeColumnsOfTable((String) mediaFilterCriteria.getMediaType());
			ArrayList<ArrayList<String>> searchedResults = ((MediaFilter) mediaFilter).getSearchResults();
			reloadTableModel(searchedResults);
		}
		else{
			System.out.println("Filter entered");
			ArrayList<ArrayList<String>> searchedResults = ((MediaFilter) mediaFilter).getSearchResults();
			reloadTableModel(searchedResults);
		}
	}

	
	
	private void reloadTableModel(ArrayList<ArrayList<String>> searchedResults){
		tableModel.clearModel();
		
		addSearchedResultsToTableModel(searchedResults);
	}
	
	private void addSearchedResultsToTableModel(ArrayList<ArrayList<String>> searchedResults){
		for(ArrayList<String> row : searchedResults){
			tableModel.addRow(row.toArray());
		}
	}
	
	private String getModelClassName() {
		return getModel().getClass().getName();
	}

	private void changeColumnsOfTable(String mediaType) {
		
		try {
			Class noParams[] = {};

			String className = getModelClassName();
			Method addDetailsToColumn = null;
			Class resultSetTableModelClass = Class.forName(className);
			Object resultSetTableModelObject = (ResultSetTableModel) getModel();
			addDetailsToColumn = resultSetTableModelClass.getDeclaredMethod(
					"add" + mediaType + "DetailsToColumn",
					noParams);
			addDetailsToColumn.invoke(((ResultSetTableModel) getModel()), null);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
