package com.view.mediaOrganiser;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class AddMediaComboBox extends JComboBox {
	static DefaultComboBoxModel addMediaComboBoxModel = new DefaultComboBoxModel(
			new Object[] { "Add Movie", "Add Book" });

	public AddMediaComboBox() {
		super(addMediaComboBoxModel);
	}


}
