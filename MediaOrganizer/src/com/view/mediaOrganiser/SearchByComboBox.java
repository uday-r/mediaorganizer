package com.view.mediaOrganiser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import com.model.domainModel.MediaFilter;
import com.model.domainModel.MediaTypeDetails;
import com.model.viewModel.ResultSetTableModel;
import com.model.viewModel.SearchByComboBoxModel;

public class SearchByComboBox extends JComboBox implements Observer {

	public SearchByComboBox() {
		super(new SearchByComboBoxModel());

	}

	

	@Override
	public void update(Observable o, Object mediaFilter) {
		MediaTypeDetails mediaFilterCriteria = ((MediaFilter) mediaFilter).getMediaFilterCriteria();
		invokeComboBoxContentChangeMethod((String) mediaFilterCriteria.getMediaType());
		
	}

	private String getModelClassName() {
		return getModel().getClass().getName();
	}

	private void invokeComboBoxContentChangeMethod(String mediaDetails) {
		Class noParams[] = {};
		Method addDetailsToColumn = null;
		try {

			String className = getModelClassName();

			Class searchByComboBoxModelClass = Class.forName(className);
			Object searchByComboBoxModelObject = (SearchByComboBoxModel) getModel();

			addDetailsToColumn = searchByComboBoxModelClass.getDeclaredMethod(
					"add" + mediaDetails + "DetailsToComboBox",
					noParams);
			addDetailsToColumn.invoke(((SearchByComboBoxModel) getModel()),
					null);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}