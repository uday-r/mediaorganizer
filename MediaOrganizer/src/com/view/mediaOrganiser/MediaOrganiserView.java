package com.view.mediaOrganiser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeListener;
import java.util.Observer;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import com.controller.editMedia.EditMediaDetails;
import com.controller.mediaOrganiser.AddMediaButtonActionListener;
import com.controller.mediaOrganiser.MediaTypeComboActionListener;
import com.controller.mediaOrganiser.ResultSetListSelectionListener;
import com.controller.mediaOrganiser.SearchByComboBoxActionListener;
import com.controller.mediaOrganiser.SearchByTextBoxFocusListener;
import com.controller.mediaOrganiser.SearchByTextBoxKeyListener;
import com.model.viewModel.ResultSetTableModel;
import com.model.viewModel.SearchByComboBoxModel;

public class MediaOrganiserView extends JFrame {

	JPanel headerPanel;
	JPanel bodyPanel;
	JPanel searchPanel;
	JPanel resultPanel;
	JPanel searchBoxPanel;
	JPanel mainPanelContainer;
	JPanel mainPanel;

	Button addMediaButton;
	RowSelectionEditButton editButton;
	JButton removeButton;
	RowSelectionSyncButton syncButton;
	MediaTypeComboBox mediaTypeComboBox;
	SearchTextField searchBox;
	SearchByComboBox searchByComboBox;
	ResultSetTable table;

	ResultSetListSelectionListener resultSetListSelectionListener;
	public MediaOrganiserView() {
		super("Media Organiser");

		drawComponents();
		setVisible(true);
		setSize(700, 500);
	}

	private void drawComponents() {
		createPanels();
		addLayoutManagerToPanels();

		createComponentsInHeaderPanel();
		addComponentsToHeaderPanel();

		searchPanel.setBorder(BorderFactory.createEmptyBorder(40, 25, 0, 25));

		createComponentsInSearchPanel();

		addComponentsToSearchBoxPanel();

		searchByComboBox.setMaximumSize(searchByComboBox.getPreferredSize());

		addComponentsToSearchPanel();
		searchBoxPanel.setAlignmentY(0);
		searchByComboBox.setAlignmentY(0);
		createComponentsInResultPanel();

		addComponentsToResultsPanel();
		addComponentsToBodyPanel();
		addComponentsToMainPanelContainer();

		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		addComponentsToMainPanel();

		bindActionListeners();
		add(mainPanel);
	}

	public String getMediaType(){
		return (String) mediaTypeComboBox.getSelectedItem();
	}
	
	public ResultSetTable getresultSetTable(){
		return this.table;
	}
	
	public String getMediaTypeProperty(){
		return (String) searchByComboBox.getSelectedItem();
	}
	
	private void addComponentsToSearchBoxPanel() {
		searchBoxPanel.add(searchBox, BorderLayout.NORTH);

	}

	private void addComponentsToSearchPanel() {
		searchPanel.add(searchBoxPanel);
		searchPanel.add(Box.createRigidArea(new Dimension(50, 10)));
		searchPanel.add(searchByComboBox);

	}

	private void addComponentsToMainPanel() {
		mainPanel.add(mainPanelContainer);

	}

	private void addComponentsToMainPanelContainer() {
		mainPanelContainer.add(headerPanel, BorderLayout.NORTH);
		mainPanelContainer.add(bodyPanel, BorderLayout.CENTER);

	}

	private void createComponentsInHeaderPanel() {
		addMediaButton = new Button("Add Media");
		editButton = new RowSelectionEditButton("Edit");
		removeButton = new JButton("Remove");
		syncButton = new RowSelectionSyncButton("Sync");
		mediaTypeComboBox = new MediaTypeComboBox();
	}

	private void createComponentsInSearchPanel() {
		searchBox = new SearchTextField(10);
		searchByComboBox = new SearchByComboBox();
	}

	private void bindActionListeners() {
		addActionListenerToAddMediaComboBox();
		addActionListenerToMediaTypeComboBox();
		addActionListenerToSearchByComboBox();
		addKeyListenerToSearchBox();
		addFocusListenerToSearchBox();
		resultSetListSelectionListener = new ResultSetListSelectionListener(table);
		activateEditButton();
		editMediaDetails();
	}

	private void editMediaDetails(){

		EditMediaDetails editMediaDetails = new EditMediaDetails();
		editButton.addActionListener(editMediaDetails);
	}
	
	private void activateEditButton(){
		
		resultSetListSelectionListener.addObserver(editButton);
		resultSetListSelectionListener.addObserver(syncButton);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.addListSelectionListener(resultSetListSelectionListener);
	}
	
	
	private void addActionListenerToAddMediaComboBox() {
		AddMediaButtonActionListener addMediaButtonActionListener = new AddMediaButtonActionListener();
		addMediaButton.addActionListener(addMediaButtonActionListener);
	}

	private void addFocusListenerToSearchBox() {
		SearchByTextBoxFocusListener searchByTextBoxFocusListener = new SearchByTextBoxFocusListener();
		searchBox.addFocusListener(searchByTextBoxFocusListener);
	}

	private void addKeyListenerToSearchBox() {
		SearchByTextBoxKeyListener searchByTextBoxKeyListener = new SearchByTextBoxKeyListener();
		searchByTextBoxKeyListener.addObserver(table);
		searchBox.addKeyListener(searchByTextBoxKeyListener);

	}

	private void addActionListenerToSearchByComboBox() {
		SearchByComboBoxActionListener searchByComboBoxActionListener = new SearchByComboBoxActionListener();
		searchByComboBoxActionListener.addObserver(searchBox);
		searchByComboBox.addActionListener(searchByComboBoxActionListener);
	}

	private void addActionListenerToMediaTypeComboBox() {

		MediaTypeComboActionListener mediaTypeComboActionListener = new MediaTypeComboActionListener();
		mediaTypeComboActionListener.addObserver(searchByComboBox);
		mediaTypeComboActionListener.addObserver(table);
		mediaTypeComboBox.addActionListener(mediaTypeComboActionListener);

	}

	private void createComponentsInResultPanel() {

		table = new ResultSetTable();
	}

	private void addComponentsToResultsPanel() {
		resultPanel.add(new JScrollPane(table));

	}

	private void addComponentsToHeaderPanel() {
		headerPanel.add(addMediaButton);
		headerPanel.add(editButton);
		headerPanel.add(removeButton);
		headerPanel.add(syncButton);
		headerPanel.add(mediaTypeComboBox);

	}

	private void addComponentsToBodyPanel() {
		bodyPanel.add(searchPanel);
		bodyPanel.add(resultPanel);

	}

	private void createPanels() {
		headerPanel = new JPanel();
		bodyPanel = new JPanel();
		searchPanel = new JPanel();
		resultPanel = new JPanel();
		searchBoxPanel = new JPanel();
		mainPanelContainer = new JPanel();
		mainPanel = new JPanel();

	}

	private void addLayoutManagerToPanels() {
		headerPanel.setLayout(new GridLayout(1, 5));
		bodyPanel.setLayout(new BoxLayout(bodyPanel, BoxLayout.Y_AXIS));
		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.X_AXIS));
		searchBoxPanel.setLayout(new BorderLayout(10, 10));
		resultPanel.setLayout(new BorderLayout());
		mainPanelContainer.setLayout(new BorderLayout());
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

	}
}
