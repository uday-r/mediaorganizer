package com.view.mediaOrganiser;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.UIManager;

public class Button extends JButton{

	public Button(String buttonLabel){
		super(buttonLabel);
		UIManager.put("Button.foreground", Color.WHITE);
		UIManager.put("Button.background", new Color(0x36c0ec));
	}
}
