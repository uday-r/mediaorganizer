package com.view.editMediaDetails;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.commons.io.FilenameUtils;

import com.Utils.fileOperations.ImageOperation;
import com.controller.editMedia.EditMovieDetailsCancelButton;
import com.model.domainModel.TableRowData;
import com.model.globalVariables.GlobalVariables;

public class EditMovieDetailsView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TableRowData tableRowData;
	private JPanel releasedLabelPanel;
	private JLabel releasedLabel;
	private JPanel releasedTextPanel;
	private JTextField released;
	private JPanel nameLabelPanel;
	private JLabel movieNameLabel;
	private JPanel FirstPanel;
	private JTextField movieName;
	private JPanel nameTextPanel;
	private JPanel headerPanel;
	private JPanel firstHeaderPanel;
	private JPanel leftFirstHeaderPanel;
	private JPanel rightFirstHeaderPanel;
	private JPanel movieNameLabelPanel;
	private JPanel movieNameTextPanel;
	private JPanel ratingLabelPanel;
	private JLabel ratingLabel;
	private JPanel ratingTextPanel;
	private JTextField rating;
	private JPanel votesLabelPanel;
	private JLabel votesLabel;
	private JPanel votesTextPanel;
	private JTextField votes;
	private JPanel secondHeaderPanel;
	private JPanel leftSecondHeaderPanel;
	private JPanel rightSecondHeaderPanel;
	private JPanel yearLabelPanel;
	private JLabel yearLabel;
	private JPanel yearTextPanel;
	private JTextField year;
	private JPanel runtimeLabelPanel;
	private JLabel runtimeLabel;
	private JPanel runtimeTextPanel;
	private JTextField runtime;
	private JPanel thirdHeaderPanel;
	private JPanel leftThirdHeaderPanel;
	private JPanel rightThirdHeaderPanel;
	private JPanel imdbIdLabelPanel;
	private JLabel imdbIdLabel;
	private JPanel imdbIdTextPanel;
	private JTextField imdbId;
	private JPanel pathLabelPanel;
	private JLabel pathLabel;
	private JPanel pathTextPanel;
	private JTextField path;
	private JPanel genreLabelPanel;
	private JComponent genreLabel;
	private JPanel genreTextPanel;
	private JTextField genre;
	private JPanel bodyPanel;
	private JPanel bodyLeftPanel;
	private JPanel genrePanel;
	private JPanel directorPanel;
	private JLabel directorLabel;
	private JTextField director;
	private JPanel writerPanel;
	private JLabel writerLabel;
	private JTextField writer;
	private JPanel actorPanel;
	private JLabel actorLabel;
	private JTextField actors;
	private JPanel plotPanel;
	private JLabel plotLabel;
	private JTextArea plot;
	private JButton getMetaData;
	private JButton save;
	private JButton cancel;
	private JPanel footerPanel;

	public EditMovieDetailsView(TableRowData tableRowData) {
		super("Edit Movie Details");
		this.tableRowData = tableRowData;
		drawComponents();
		addTableRowDataToComponents();
		bindActionListeners();
		setVisible(true);
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}

	private void drawComponents() {
		headerPanelCreation();
		addComponentsToHeaderpanel();
		bodypanelCreation();
		addComponentsToBodyPanel();
	}

	private void bindActionListeners(){
		cancelButtonListener();
	}
	
	private void cancelButtonListener() {
		EditMovieDetailsCancelButton editMovieDetailsCancelButton = new EditMovieDetailsCancelButton();
		cancel.addActionListener(editMovieDetailsCancelButton);
	}
	
	private void addTableRowDataToComponents() {
		movieName.setText(tableRowData.getName());
		year.setText(Integer.toString(tableRowData.getYear()));
		imdbId.setText(tableRowData.getImdbId());
		rating.setText(String.valueOf(tableRowData.getRating()));
		released.setText(tableRowData.getReleaseData());
		path.setText(tableRowData.getMediaOrganizerFilePath());
		votes.setText(tableRowData.getRatingCount());
		runtime.setText(tableRowData.getMovielength());
		genre.setText(tableRowData.getGenre());
		director.setText(tableRowData.getDirectors());
		writer.setText(tableRowData.getWriters());
		actors.setText(tableRowData.getActors());
		plot.setText(tableRowData.getPlot());

	}

	private JPanel footerPanel() {

		JPanel returnPostBodyPanel = new JPanel();
		returnPostBodyPanel.setLayout(new BoxLayout(returnPostBodyPanel,
				BoxLayout.X_AXIS));
		returnPostBodyPanel.setBorder(BorderFactory.createEmptyBorder(10, 10,
				0, 10));
		getMetaData = new JButton("Get Metadata");

		save = new JButton("Save");

		cancel = new JButton("Cancel");

		returnPostBodyPanel.add(Box.createHorizontalGlue());
		returnPostBodyPanel.add(getMetaData);
		returnPostBodyPanel.add(Box.createHorizontalGlue());
		returnPostBodyPanel.add(save);
		returnPostBodyPanel.add(Box.createHorizontalGlue());
		returnPostBodyPanel.add(cancel);
		returnPostBodyPanel.add(Box.createHorizontalGlue());
		return returnPostBodyPanel;
	}

	private void bodypanelCreation() {
		bodyPanel = new JPanel();
		add(bodyPanel, BorderLayout.CENTER);
		bodyPanel.setLayout(new BoxLayout(bodyPanel, BoxLayout.X_AXIS));
		bodyPanel.setBorder(BorderFactory.createEmptyBorder(4, 10, 0, 10));

	}

	private void headerPanelCreation() {
		headerPanel = new JPanel();
		add(headerPanel, BorderLayout.NORTH);
		headerPanel.setLayout(new GridLayout(1, 3, 20, 0));
		headerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
	}

	private void addComponentsToBodyPanel() {
		addComponentsToBodyLeftPanel();
		addComponentsToBodyRightPanel();
	}

	private void addComponentsToBodyLeftPanel() {

		
		
		File movieFolder = new File(tableRowData.getMediaOrganizerFilePath());
		System.out.println("File exists : "+tableRowData.getMediaOrganizerFilePath());
		ImageIcon mediaIcon = null;
		if (movieFolder.exists()) {
			BufferedImage bufferedPoster;
			File[] listOfMovies = movieFolder.listFiles();

			try {
				for (File file : listOfMovies) {

					String fileName = file.getName();

					System.out.println(GlobalVariables.getMediaOrganizerPath()+"Movie/"+FilenameUtils.removeExtension(fileName)+"/"+fileName);
					String fileTypeDetails = URLConnection.guessContentTypeFromName(GlobalVariables.getMediaOrganizerPath()+ GlobalVariables.getMovieFolder() +FilenameUtils.removeExtension(fileName)+"/"+fileName);
					if (fileTypeDetails == null) {
						fileTypeDetails = MimetypesFileTypeMap.getDefaultFileTypeMap()
								.getContentType(fileName);
					}
					String type = fileTypeDetails.split("/")[0];
					if (type.equals("image")) {
						bufferedPoster = ImageIO.read(new File(movieFolder.getAbsolutePath()+"/"+fileName));
						mediaIcon = new ImageIcon(bufferedPoster);
					}

				}
				if(mediaIcon == null){
					System.out.println("MediaIcon is null");

					mediaIcon = ImageOperation.createImageIcon("Images/default.jpg");	
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Image mediaImage = ImageOperation.scaleImage(mediaIcon, 650);
			mediaIcon = new ImageIcon(mediaImage);
			JLabel mediaPoster = new JLabel(mediaIcon);

			mediaPoster.setAlignmentY(CENTER_ALIGNMENT);
			bodyLeftPanel = new JPanel();
			bodyLeftPanel.setLayout(new BoxLayout(bodyLeftPanel, BoxLayout.X_AXIS));
			bodyLeftPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 12, 10));
			bodyPanel.add(bodyLeftPanel);
			bodyLeftPanel.add(mediaPoster);
		}
		else{
			
			new MediaFileMissing(movieFolder.getName());
		}
		
	
		
	}

	private void addComponentsToHeaderpanel() {
		createFirstHeaderPanel();
		createSecondHeaderPanel();
		createThirdHeaderPanel();
	}

	private void createFirstHeaderPanel() {
		firstHeaderPanel = new JPanel();
		headerPanel.add(firstHeaderPanel);
		firstHeaderPanel.setLayout(new BoxLayout(firstHeaderPanel,
				BoxLayout.X_AXIS));

		leftFirstHeaderPanel = new JPanel();
		firstHeaderPanel.add(leftFirstHeaderPanel);
		leftFirstHeaderPanel.setLayout(new BoxLayout(leftFirstHeaderPanel,
				BoxLayout.Y_AXIS));

		movieNameLabelPanel = new JPanel();
		movieNameLabelPanel.setLayout(new BoxLayout(movieNameLabelPanel,
				BoxLayout.Y_AXIS));

		movieNameLabel = new JLabel("Movie Name:");
		movieNameLabel.setAlignmentX(RIGHT_ALIGNMENT);
		movieNameLabelPanel.add(movieNameLabel);
		leftFirstHeaderPanel.add(movieNameLabelPanel);

		leftFirstHeaderPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		ratingLabelPanel = new JPanel();
		ratingLabelPanel.setLayout(new BoxLayout(ratingLabelPanel,
				BoxLayout.Y_AXIS));

		ratingLabel = new JLabel("Rating:");
		ratingLabel.setAlignmentX(RIGHT_ALIGNMENT);
		ratingLabelPanel.add(ratingLabel);
		leftFirstHeaderPanel.add(ratingLabelPanel);

		leftFirstHeaderPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		votesLabelPanel = new JPanel();
		votesLabelPanel.setLayout(new BoxLayout(votesLabelPanel,
				BoxLayout.Y_AXIS));

		votesLabel = new JLabel("Votes:");
		votesLabel.setAlignmentX(RIGHT_ALIGNMENT);
		votesLabelPanel.add(votesLabel);
		leftFirstHeaderPanel.add(votesLabelPanel);

		rightFirstHeaderPanel = new JPanel();
		firstHeaderPanel.add(rightFirstHeaderPanel);
		rightFirstHeaderPanel.setLayout(new BoxLayout(rightFirstHeaderPanel,
				BoxLayout.Y_AXIS));

		movieNameTextPanel = new JPanel();
		movieNameTextPanel.setLayout(new BoxLayout(movieNameTextPanel,
				BoxLayout.Y_AXIS));
		movieName = new JTextField();
		movieName.setAlignmentX(LEFT_ALIGNMENT);
		movieNameTextPanel.add(movieName);
		rightFirstHeaderPanel.add(movieNameTextPanel);

		rightFirstHeaderPanel.add(Box.createRigidArea(new Dimension(0, 5)));

		ratingTextPanel = new JPanel();
		ratingTextPanel.setLayout(new BoxLayout(ratingTextPanel,
				BoxLayout.Y_AXIS));
		rating = new JTextField();
		rating.setAlignmentX(LEFT_ALIGNMENT);
		ratingTextPanel.add(rating);
		rightFirstHeaderPanel.add(ratingTextPanel);

		rightFirstHeaderPanel.add(Box.createRigidArea(new Dimension(0, 5)));

		votesTextPanel = new JPanel();
		votesTextPanel
				.setLayout(new BoxLayout(votesTextPanel, BoxLayout.Y_AXIS));
		votes = new JTextField();
		votes.setAlignmentX(LEFT_ALIGNMENT);
		votesTextPanel.add(votes);

		rightFirstHeaderPanel.add(votesTextPanel);
	}

	private void createSecondHeaderPanel() {
		secondHeaderPanel = new JPanel();
		headerPanel.add(secondHeaderPanel);
		secondHeaderPanel.setLayout(new BoxLayout(secondHeaderPanel,
				BoxLayout.X_AXIS));

		leftSecondHeaderPanel = new JPanel();
		secondHeaderPanel.add(leftSecondHeaderPanel);
		leftSecondHeaderPanel.setLayout(new BoxLayout(leftSecondHeaderPanel,
				BoxLayout.Y_AXIS));
		leftSecondHeaderPanel.setAlignmentX(TOP_ALIGNMENT);

		yearLabelPanel = new JPanel();
		yearLabelPanel
				.setLayout(new BoxLayout(yearLabelPanel, BoxLayout.Y_AXIS));
		yearLabel = new JLabel("Year:");
		yearLabel.setAlignmentX(RIGHT_ALIGNMENT);
		yearLabelPanel.add(yearLabel);
		leftSecondHeaderPanel.add(yearLabelPanel);

		leftSecondHeaderPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		releasedLabelPanel = new JPanel();
		releasedLabelPanel.setLayout(new BoxLayout(releasedLabelPanel,
				BoxLayout.Y_AXIS));
		releasedLabel = new JLabel("Released:");
		releasedLabel.setAlignmentX(BOTTOM_ALIGNMENT);
		releasedLabel.setAlignmentX(RIGHT_ALIGNMENT);
		releasedLabelPanel.add(releasedLabel);
		leftSecondHeaderPanel.add(releasedLabelPanel);

		leftSecondHeaderPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		runtimeLabelPanel = new JPanel();
		runtimeLabelPanel.setLayout(new BoxLayout(runtimeLabelPanel,
				BoxLayout.Y_AXIS));
		runtimeLabel = new JLabel("Runtime:");
		runtimeLabel.setAlignmentX(RIGHT_ALIGNMENT);
		runtimeLabelPanel.add(runtimeLabel);
		leftSecondHeaderPanel.add(runtimeLabelPanel);

		rightSecondHeaderPanel = new JPanel();
		secondHeaderPanel.add(rightSecondHeaderPanel);
		rightSecondHeaderPanel.setLayout(new BoxLayout(rightSecondHeaderPanel,
				BoxLayout.Y_AXIS));

		yearTextPanel = new JPanel();
		yearTextPanel.setLayout(new BoxLayout(yearTextPanel, BoxLayout.Y_AXIS));
		year = new JTextField();
		year.setAlignmentX(LEFT_ALIGNMENT);
		yearTextPanel.add(year);
		rightSecondHeaderPanel.add(yearTextPanel);

		rightSecondHeaderPanel.add(Box.createRigidArea(new Dimension(0, 5)));

		releasedTextPanel = new JPanel();
		releasedTextPanel.setLayout(new BoxLayout(releasedTextPanel,
				BoxLayout.Y_AXIS));
		released = new JTextField();
		released.setAlignmentX(LEFT_ALIGNMENT);
		releasedTextPanel.add(released);
		rightSecondHeaderPanel.add(releasedTextPanel);

		rightSecondHeaderPanel.add(Box.createRigidArea(new Dimension(0, 5)));

		runtimeTextPanel = new JPanel();
		runtimeTextPanel.setLayout(new BoxLayout(runtimeTextPanel,
				BoxLayout.Y_AXIS));
		runtime = new JTextField();
		runtime.setAlignmentX(LEFT_ALIGNMENT);
		runtimeTextPanel.add(runtime);

		rightSecondHeaderPanel.add(runtimeTextPanel);
	}

	private void createThirdHeaderPanel() {
		thirdHeaderPanel = new JPanel();
		headerPanel.add(thirdHeaderPanel);
		thirdHeaderPanel.setLayout(new BoxLayout(thirdHeaderPanel,
				BoxLayout.X_AXIS));

		leftThirdHeaderPanel = new JPanel();
		thirdHeaderPanel.add(leftThirdHeaderPanel);
		leftThirdHeaderPanel.setLayout(new BoxLayout(leftThirdHeaderPanel,
				BoxLayout.Y_AXIS));

		leftThirdHeaderPanel.setAlignmentX(TOP_ALIGNMENT);

		imdbIdLabelPanel = new JPanel();
		imdbIdLabelPanel.setLayout(new BoxLayout(imdbIdLabelPanel,
				BoxLayout.Y_AXIS));
		imdbIdLabel = new JLabel("ImdbId:");
		imdbIdLabel.setAlignmentX(RIGHT_ALIGNMENT);
		imdbIdLabelPanel.add(imdbIdLabel);
		leftThirdHeaderPanel.add(imdbIdLabelPanel);

		leftThirdHeaderPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		pathLabelPanel = new JPanel();
		pathLabelPanel
				.setLayout(new BoxLayout(pathLabelPanel, BoxLayout.Y_AXIS));
		pathLabel = new JLabel("Path:");
		pathLabel.setAlignmentX(RIGHT_ALIGNMENT);
		pathLabelPanel.add(pathLabel);
		leftThirdHeaderPanel.add(pathLabelPanel);

		leftThirdHeaderPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		genreLabelPanel = new JPanel();
		genreLabelPanel.setLayout(new BoxLayout(genreLabelPanel,
				BoxLayout.Y_AXIS));
		genreLabel = new JLabel("Genre:");
		genreLabel.setAlignmentX(RIGHT_ALIGNMENT);
		genreLabelPanel.add(genreLabel);
		leftThirdHeaderPanel.add(genreLabelPanel);

		rightThirdHeaderPanel = new JPanel();
		thirdHeaderPanel.add(rightThirdHeaderPanel);
		rightThirdHeaderPanel.setLayout(new BoxLayout(rightThirdHeaderPanel,
				BoxLayout.Y_AXIS));

		imdbIdTextPanel = new JPanel();
		imdbIdTextPanel.setLayout(new BoxLayout(imdbIdTextPanel,
				BoxLayout.Y_AXIS));
		imdbId = new JTextField();
		imdbId.setAlignmentX(LEFT_ALIGNMENT);
		imdbIdTextPanel.add(imdbId);
		rightThirdHeaderPanel.add(imdbIdTextPanel);

		rightThirdHeaderPanel.add(Box.createRigidArea(new Dimension(0, 5)));

		pathTextPanel = new JPanel();
		pathTextPanel.setLayout(new BoxLayout(pathTextPanel, BoxLayout.Y_AXIS));
		path = new JTextField();
		path.setAlignmentX(LEFT_ALIGNMENT);
		pathTextPanel.add(path);
		rightThirdHeaderPanel.add(pathTextPanel);

		rightThirdHeaderPanel.add(Box.createRigidArea(new Dimension(0, 5)));

		genreTextPanel = new JPanel();
		genreTextPanel
				.setLayout(new BoxLayout(genreTextPanel, BoxLayout.Y_AXIS));
		genre = new JTextField();
		genre.setAlignmentX(LEFT_ALIGNMENT);
		genreTextPanel.add(genre);
		rightThirdHeaderPanel.add(genreTextPanel);
	}

	private void addComponentsToBodyRightPanel() {

		JPanel bodyRightPanel = new JPanel();
		bodyPanel.add(bodyRightPanel);
		bodyRightPanel
				.setLayout(new BoxLayout(bodyRightPanel, BoxLayout.Y_AXIS));
		bodyRightPanel.setBorder(BorderFactory.createEmptyBorder(8, 0, 10, 0));
		bodyRightPanel.setAlignmentX(CENTER_ALIGNMENT);

		directorPanel = new JPanel();
		directorPanel.setLayout(new BoxLayout(directorPanel, BoxLayout.Y_AXIS));
		directorPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		directorLabel = new JLabel("Director: ");
		director = new JTextField();
		director.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));

		JPanel directorLabelPanel = new JPanel();
		directorLabelPanel.setLayout(new BoxLayout(directorLabelPanel,
				BoxLayout.X_AXIS));
		directorLabelPanel.add(directorLabel);
		directorLabelPanel.add(Box.createHorizontalGlue());
		directorPanel.add(directorLabelPanel);

		directorPanel.add(director);

		writerPanel = new JPanel();
		writerPanel.setLayout(new BoxLayout(writerPanel, BoxLayout.Y_AXIS));
		writerPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		writerLabel = new JLabel("Writer: ");
		writer = new JTextField();
		writer.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));

		JPanel writerLabelPanel = new JPanel();
		writerLabelPanel.setLayout(new BoxLayout(writerLabelPanel,
				BoxLayout.X_AXIS));
		writerLabelPanel.add(writerLabel);
		writerLabelPanel.add(Box.createHorizontalGlue());
		writerPanel.add(writerLabelPanel);
		writerPanel.add(writer);

		actorPanel = new JPanel();
		actorPanel.setLayout(new BoxLayout(actorPanel, BoxLayout.Y_AXIS));
		actorPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		actorLabel = new JLabel("Actors: ");
		actors = new JTextField();
		actors.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));

		JPanel actorLabelPanel = new JPanel();
		actorLabelPanel.setLayout(new BoxLayout(actorLabelPanel,
				BoxLayout.X_AXIS));
		actorLabelPanel.add(actorLabel);
		actorLabelPanel.add(Box.createHorizontalGlue());
		actorPanel.add(actorLabelPanel);
		actorPanel.add(actors);

		plotPanel = new JPanel();
		plotPanel.setLayout(new BoxLayout(plotPanel, BoxLayout.Y_AXIS));
		plotPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		plotLabel = new JLabel("Plot: ");
		plot = new JTextArea(2, 5);

		JPanel plotLabelPanel = new JPanel();
		plotLabelPanel
				.setLayout(new BoxLayout(plotLabelPanel, BoxLayout.X_AXIS));
		plotLabelPanel.add(plotLabel);
		plotLabelPanel.add(Box.createHorizontalGlue());
		plotPanel.add(plotLabelPanel);

		plotPanel.add(new JScrollPane(plot));

		footerPanel = footerPanel();

		bodyRightPanel.add(directorPanel);
		bodyRightPanel.add(writerPanel);
		bodyRightPanel.add(actorPanel);
		bodyRightPanel.add(plotPanel);
		bodyRightPanel.add(footerPanel);

	}

	public JPanel getNameLabelPanel() {
		return nameLabelPanel;
	}

	public void setNameLabelPanel(JPanel nameLabelPanel) {
		this.nameLabelPanel = nameLabelPanel;
	}

	public JPanel getFirstPanel() {
		return FirstPanel;
	}

	public void setFirstPanel(JPanel firstPanel) {
		FirstPanel = firstPanel;
	}

	public JPanel getNameTextPanel() {
		return nameTextPanel;
	}

	public void setNameTextPanel(JPanel nameTextPanel) {
		this.nameTextPanel = nameTextPanel;
	}

	public JPanel getGenrePanel() {
		return genrePanel;
	}

	public void setGenrePanel(JPanel genrePanel) {
		this.genrePanel = genrePanel;
	}

}
