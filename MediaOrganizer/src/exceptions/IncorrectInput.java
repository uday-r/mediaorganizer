package exceptions;

public class IncorrectInput extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public IncorrectInput(){
		super();
	}
	
	public IncorrectInput(String message){
		super(message);
		this.message = message;
	}

	@Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }

}
