package exceptions;

public class ConnectionNotAvaliableException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ConnectionNotAvaliableException(String message) {
        super(message);
    }

    public ConnectionNotAvaliableException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
