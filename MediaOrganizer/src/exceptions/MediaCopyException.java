package exceptions;

public class MediaCopyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	private Throwable e;
	public MediaCopyException(){
		super();
	}

	public MediaCopyException(String message){
		super(message);
		this.message = message;
	}
	
	public MediaCopyException(String message ,Throwable e){
		super(message, e);
		this.message = message;
		this.setE(e);
	}
	
	@Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }

	public Throwable getE() {
		return e;
	}

	public void setE(Throwable e) {
		this.e = e;
	}
}
