package exceptions;

public class FileAlreadyFound extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public FileAlreadyFound(){
		super();
	}
	
	public FileAlreadyFound(String message){
		super(message);
		this.message = message;
	}

	@Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }

}
